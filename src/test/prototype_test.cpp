/*!
 * \file prototype_test.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 24, 2017
 */

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
namespace utf = boost::unit_test;

#include <boost/test/unit_test_parameters.hpp>
namespace but = boost::unit_test;

#include <experimental/random>
#include <string>

#include "../main/model/prototype.hpp"
using namespace two_dim_skp;

#include "tools/instance_manager.hpp"
#include "tools/auxiliary_functions.hpp"
#include "tools/io_config.hpp"
using namespace two_dim_skp_test;


BOOST_AUTO_TEST_SUITE(prototype_test,
		      * utf::disabled()
		      * utf::depends_on ("data_file"))
//BOOST_AUTO_TEST_SUITE(prototype_test,
//		      * utf::depends_on ("data_file"))

// copy and clone prototypes correctly
//
BOOST_FIXTURE_TEST_CASE(copy_and_clone_prototype, INSTANCE_MANAGER)
{
  but::unit_test_log.set_threshold_level( but::log_messages );
  setup(BEASLEY_FILENAMES[0],"");

  BOOST_TEST_MESSAGE("copy_and_clone_prototype " << std::string (30, '-'));

  auto &prototype = random_prototype(prototypes);
  BOOST_TEST_MESSAGE("chosen prototype: " << prototype);

  auto copy = prototype;
  BOOST_TEST((copy.get_cur_pieces () == 0 && prototype.get_cur_pieces () == 0));
}

// clone prototypes until an exception is thrown
//
BOOST_FIXTURE_TEST_CASE(exaust_clone_prototype, INSTANCE_MANAGER)
{
  but::unit_test_log.set_threshold_level( but::log_messages );
  setup(BEASLEY_FILENAMES[0],"");
  BOOST_TEST_MESSAGE("exaust_clone_prototype " << std::string (30, '-'));

  PROTOTYPE &prototype = random_prototype(prototypes);
  BOOST_TEST_MESSAGE("chosen prototype: " << prototype);

  BOOST_REQUIRE_THROW(
      while (true) { prototype.place (0,0); }, std::runtime_error);
}

// print pieces to *.svg
//
BOOST_FIXTURE_TEST_CASE(print_to_svg, INSTANCE_MANAGER)
{
  but::unit_test_log.set_threshold_level( but::log_messages );
  setup(BEASLEY_FILENAMES[0],"");
  BOOST_TEST_MESSAGE("print_to_svg " << std::string (30, '-'));

  for (const auto& e : prototypes)
    {
      const PROTOTYPE& p = e.second;

      uint x = abs ((int) (p.get_length () - bin_length));
      x = x == 0 ? 0 : rand () % x + 1;

      uint y = abs ((int) (p.get_height () - bin_height));
      y = y == 0 ? 0 : rand () % y + 1;

      std::ofstream os ("piece_" + std::to_string (p.get_id ()) + ".svg");
//	print (width, height, p, x, y, os, 10.0f);
      os.close ();
    }
  BOOST_TEST(true);
}

BOOST_AUTO_TEST_SUITE_END() /* prototype */
