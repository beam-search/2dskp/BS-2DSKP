/*!
 * \file data_file_test.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 24, 2017
 */

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
namespace utf = boost::unit_test;

#include <boost/test/data/test_case.hpp>
#include <boost/test/data/monomorphic.hpp>
namespace bdata = boost::unit_test::data;

#include <experimental/random>
#include <cstdlib>
#include <ctime>

#include "../main/model/prototype.hpp"
#include "../main/types.hpp"
using namespace two_dim_skp;

#include "tools/auxiliary_functions.hpp"
#include "tools/io_config.hpp"

//BOOST_AUTO_TEST_SUITE(data_file)
BOOST_AUTO_TEST_SUITE(data_file, * utf::disabled())

  BOOST_AUTO_TEST_CASE(random)
  {
    // getting random prototype

    std::srand (std::time (0));
    auto random_index = rand ()
	% (sizeof(BEASLEY_FILENAMES) / sizeof(std::string));

    uint bin_length;
    uint bin_height;
    PROTOTYPES prototypes;
    read_instance (BEASLEY_FILENAMES[random_index], bin_length, bin_height,
		   prototypes);

    BOOST_TEST_MESSAGE(
	BEASLEY_FILENAMES[random_index] <<
	", " "bin width = " << bin_length <<
	", " "bin height = " << bin_height);

    for (auto &value : prototypes)
      {
	BOOST_TEST_MESSAGE(value.second);
      }

    BOOST_TEST(true);
  }

  BOOST_DATA_TEST_CASE(custom, bdata::make(BEASLEY_FILENAMES[2]), filename){
  uint bin_length;
  uint bin_height;
  PROTOTYPES prototypes;

//  read_instance (filename, bin_length, bin_height, prototypes);
  read_instance (BEASLEY_FILENAMES[2], bin_length, bin_height, prototypes);

  BOOST_TEST_MESSAGE(filename << ", "
      "bin width = " << bin_length << ", "
      "bin height = " << bin_height);

  for (auto &value : prototypes)
    {
      BOOST_TEST_MESSAGE(value.second);
    }

  BOOST_TEST(true);
}

BOOST_AUTO_TEST_SUITE_END() /* data_file */
