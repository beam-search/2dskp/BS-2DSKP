/*!
 * \file data_files.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Oct 6, 2018
 */

#ifndef IO_CONFIG_HPP_
#define IO_CONFIG_HPP_

#include <string>

// data and results directory

const std::string result_dir = "/home/calegria/Software/development/cpp/BS-BPP/BS-2DSKP/output/";
const std::string data_dir   = "/home/calegria/Software/development/cpp/BS-BPP/BS-2DSKP/data/";

// Beasley problem instances

const std::string BEASLEY_INSTANCE_NAMES[] =
    {
	"beasley1",
	"beasley2",
	"beasley3",
	"beasley4",
	"beasley5",
	"beasley6",
	"beasley7",
	"beasley8",
	"beasley9",
	"beasley10",
	"beasley11",
	"beasley12"
    };
const std::string BEASLEY_FILENAMES[] =
    {
	data_dir + "beasley/beasley1.2kp",
	data_dir + "beasley/beasley2.2kp",
	data_dir + "beasley/beasley3.2kp",
	data_dir + "beasley/beasley4.2kp",
	data_dir + "beasley/beasley5.2kp",
	data_dir + "beasley/beasley6.2kp",
	data_dir + "beasley/beasley7.2kp",
	data_dir + "beasley/beasley8.2kp",
	data_dir + "beasley/beasley9.2kp",
	data_dir + "beasley/beasley10.2kp",
	data_dir + "beasley/beasley11.2kp",
	data_dir + "beasley/beasley12.2kp"
    };
const uint BEASLEY_OPTIMAL_SOLUTIONS[] =
    {
	164,
	230,
	247,
	268,
	358,
	289,
	430,
	834,
	924,
	1452,
	1688,
	1865
    };

// Okp problem instances

const std::string OKP_INSTANCE_NAMES[] =
    {
	"okp1",
	"okp2",
	"okp3",
	"okp4",
	"okp5"
    };
const std::string OKP_FILENAMES[] =
    {
	data_dir + "okp/okp1.2kp",
	data_dir + "okp/okp2.2kp",
	data_dir + "okp/okp3.2kp",
	data_dir + "okp/okp4.2kp",
	data_dir + "okp/okp5.2kp"
    };
const uint OKP_OPTIMAL_SOLUTIONS[] =
    {
	27718,
	22502,
	24019,
	32893,
	27923
    };

// Hadchr problem instances

const std::string HADCHR_INSTANCE_NAMES[] =
    {
	"hadchr-3",
	"hadchr-11"
    };
const std::string HADCHR_FILENAMES[] =
    {
	data_dir + "hadchr/hadchr-3.2kp",
	data_dir + "hadchr/hadchr-11.2kp"
    };
const uint HADCHR_OPTIMAL_SOLUTIONS[] =
    {
	1178,
	1270
    };

// Wang problem instances

const std::string WANG_INSTANCE_NAMES[] =
    {
	"wang20"
    };
const std::string WANG_FILENAMES[] =
    {
	data_dir + "wang20.2kp"
    };
const uint WANG_OPTIMAL_SOLUTIONS[] =
    {
	2726
    };

// CGcut problem instances

const std::string CGCUT_INSTANCE_NAMES[] =
    {
	"cgcut3"
    };
const std::string CGCUT_FILENAMES[] =
    {
	data_dir + "cgcut3.2kp"
    };
const uint CGCUT_OPTIMAL_SOLUTIONS[] =
    {
	1860
    };

#endif /* IO_CONFIG_HPP_ */
