/*!
 * \file test_tools.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 24, 2017
 */

#include <boost/test/unit_test.hpp>
#include <iterator>
#include <numeric>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include "../../main/tools.hpp"
#include "auxiliary_functions.hpp"
#include "io_config.hpp"

const PROTOTYPES::key_type&
random_key(PROTOTYPES& prototypes)
{
  std::srand(std::time(nullptr));
  auto ptr = prototypes.begin();
  std::advance(ptr, rand() % prototypes.size());

  return (*ptr).first;
}

PROTOTYPES::mapped_type&
random_prototype(PROTOTYPES& prototypes)
{
  return prototypes[random_key(prototypes)];
}

void
read_instance (const std::string& instance_filename, uint &length, uint &height,
	       PROTOTYPES &prototypes)
{
  std::ifstream is (instance_filename, std::ifstream::in);
  BOOST_REQUIRE(is.is_open ());

  readFrom2KPFile (is, length, height, prototypes);
  BOOST_REQUIRE_NO_THROW(is.close());

  BOOST_TEST_REQUIRE(length > 0, "bin length cannot be negative");
  BOOST_TEST_REQUIRE(height > 0, "bin height cannot be negative");
  BOOST_TEST_REQUIRE(!prototypes.empty (), "there should be at least one prototype");
}

// print the resulting pieces to an *.svg file

void
print_bin (const std::string& bin_name, uint bin_length, uint bin_height,
	   const BIN& bin)
{
  std::string svg_file = result_dir + bin_name + ".svg";
  BOOST_TEST_MESSAGE("writing bin to svg '" + svg_file + "'");

  std::ofstream os (svg_file);
  writeToSolFile (os, 30, bin_length, bin_height, bin);
  os.close ();
}

void
append_result (const std::string& instance_name, const PROTOTYPES& prototypes,
	       const BIN& result)
{
  std::string text_file = result_dir + "results.txt";
  BOOST_TEST_MESSAGE("appending result to txt '" + text_file + "'");

  uint profit = std::accumulate (
      result.begin (), result.end (), (uint) 0,
      [] (uint &a, const PIECE& b)
      {
	return a + b.get_profit();
      });
  uint max_profit = std::accumulate (
      prototypes.begin (), prototypes.end (), (uint) 0,
      [] (uint &a, const PROTOTYPES::value_type& b)
      {
	return a + b.second.get_max_pieces() * b.second.get_profit();
      });
  uint max_pieces = std::accumulate (
      prototypes.begin (), prototypes.end (), (uint) 0,
      [] (uint &a, const PROTOTYPES::value_type& b)
      {
	return a + b.second.get_max_pieces();
      });

  std::ofstream os (text_file, std::ios::out | std::ios::app);
  os << instance_name << ","
      << prototypes.size() << ","
      << max_pieces << ","
      << max_profit << ","
      << result.size() << ","
      << profit << std::endl;
  os.close ();
}
