/*!
 * \file instance_manager.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jun 22, 2018
 */

#ifndef INSTANCE_MANAGER_HPP_
#define INSTANCE_MANAGER_HPP_

#include <string>

#include "auxiliary_functions.hpp"

#include "../../main/types.hpp"
using namespace two_dim_skp;

namespace two_dim_skp_test
{

  class INSTANCE_MANAGER
  {
  public:
    INSTANCE_MANAGER () :
	bin_length (0), bin_height (0), prototypes ({{ } })
    {
    }
    virtual
    ~INSTANCE_MANAGER ()
    {
    }

    virtual void
    setup (const std::string& instance_filename,
	  const std::string& instance_name)
    {
      this->prototypes.clear ();
      this->instance_name = instance_name;
      read_instance (instance_filename, bin_length, bin_height, prototypes);
    }

    uint bin_length;
    uint bin_height;
    std::string instance_name;
    PROTOTYPES prototypes;
  };

} /* namespace two_dim_skp_test */

#endif /* INSTANCE_MANAGER_HPP_ */
