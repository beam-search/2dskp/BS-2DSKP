/*!
 * \file tools.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 24, 2017
 */

#ifndef AUXILIARY_FUNCTIONS_HPP_
#define AUXILIARY_FUNCTIONS_HPP_

#include <string>
#include <vector>

#include "../../main/model/piece.hpp"
#include "../../main/types.hpp"
using namespace two_dim_skp;


// get a random PROTOTYPE from the given PROTOTYPES map

const PROTOTYPES::key_type&
random_key(PROTOTYPES& prototypes);

PROTOTYPES::mapped_type&
random_prototype(PROTOTYPES& prototypes);

// read the given instance

void
read_instance (const std::string& instance_filename, uint& length, uint& height,
	       PROTOTYPES& prototypes);

// print the given bin (a PIECE vector) to an *.svg file

void
print_bin (const std::string& result_filename, uint bin_length, uint bin_height,
	   const BIN& bin);

// append statistics about the given solution the the given filename

void
append_result (const std::string& result_filename, const PROTOTYPES& prototypes,
	       const BIN& bin);

#endif /* AUXILIARY_FUNCTIONS_HPP_ */
