/*!
 * \file test.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 17, 2017
 */

#define BOOST_TEST_MODULE BS 2D-SKP
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <iostream>

// Turn off std::cout buffering. It makes tests running progress to show more
// accurate.
//
static struct DisableStdCoutBuffering
{
  DisableStdCoutBuffering ()
  {
    std::cout.setf (std::ios_base::unitbuf);
  }
} DisableStdCoutBuffering;
