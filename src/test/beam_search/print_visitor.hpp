/*!
 * \file print_visitor.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Oct 8, 2018
 */

#ifndef PRINT_VISITOR_HPP_
#define PRINT_VISITOR_HPP_

#include <fstream>
#include <string>

#include "../../main/skp/skp_filter.hpp"
#include "../../main/types.hpp"
#include "../../main/tools.hpp"
using namespace two_dim_skp;

#define SCALE 30

namespace two_dim_skp_test
{

  class PRINT_VISITOR
  {
  public:
    PRINT_VISITOR (uint length, uint height, const std::string& directory,
		   const std::string& filename) :
	length (length), height (height), directory (directory),
	filename (filename)
    {
    }

    void
    visit_node (const SKP_BS_FILTER::NODE_TYPE& node,
		unsigned long node_id,
		const SKP_BS_FILTER::LOCAL_EVAL_TYPE& local_eval,
		const SKP_BS_FILTER::GLOBAL_EVAL_TYPE& global_eval)
    {
      // print node

      {
	std::ofstream os (
	    directory + filename + "__" +
	    std::to_string (node_id) + "__bin.svg");
	writeToSolFile (os, SCALE, length, height, node.get_bin ());
	os.close ();
      }

#ifndef GE_MIN_KNAP
      // print projected node

      {
	std::ofstream os (
	    directory + filename + "__" +
	    std::to_string (node_id) + "__projected-bin.svg");
	writeToSolFile (os, SCALE, length, height, global_eval.get_bin ());
	os.close ();
      }
#endif
    }

  private:

    const uint length;
    const uint height;
    const std::string& directory;
    const std::string& filename;

  };

} /* namespace two_dim_skp_test */

#endif /* PRINT_VISITOR_HPP_ */
