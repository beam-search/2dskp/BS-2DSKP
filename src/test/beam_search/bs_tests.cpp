/*!
 * \file single_instance__several_alphas.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 9, 2018
 */

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
namespace utf = boost::unit_test;

#include <boost/test/data/test_case.hpp>
#include <boost/test/data/monomorphic.hpp>
namespace bdata = boost::unit_test::data;

#include <boost/filesystem.hpp>
namespace bfs = boost::filesystem;

#include <boost/test/unit_test_parameters.hpp>
namespace but = boost::unit_test;

#include <boost/date_time/posix_time/posix_time.hpp>

#include <experimental/random>
#include <string>

#include "../../main/beam_search/beam_search.hpp"
#include "../../main/skp/skp_filter.hpp"
#include "../../main/tools.hpp"
using namespace two_dim_skp;

#include "../tools/auxiliary_functions.hpp"
#include "../tools/instance_manager.hpp"
#include "../tools/io_config.hpp"
#include "bs_instance_manager.hpp"
#include "print_visitor.hpp"
#include "empty_visitor.hpp"
using namespace two_dim_skp_test;

//BOOST_AUTO_TEST_SUITE(analysis, * utf::disabled ())
BOOST_AUTO_TEST_SUITE(analysis)

// =============================================================================
// a single instance with several pairs (\alpha,\beta)
// =============================================================================

#define INSTANCE_NAME		  BEASLEY_INSTANCE_NAMES[7]
#define INSTANCE_FILENAME	  BEASLEY_FILENAMES[7]
#define INSTANCE_OPTIMAL_SOLUTION BEASLEY_OPTIMAL_SOLUTIONS[7]

//#define INSTANCE_NAMES	           OKP_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   OKP_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS OKP_OPTIMAL_SOLUTIONS

//#define INSTANCE_NAMES		   HADCHR_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   HADCHR_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS HADCHR_OPTIMAL_SOLUTIONS

//#define INSTANCE_NAMES		   WANG_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   WANG_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS WANG_OPTIMAL_SOLUTIONS

//#define INSTANCE_NAMES		   CGCUT_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   CGCUT_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS CGCUT_OPTIMAL_SOLUTIONS

//BOOST_TEST_DECORATOR(* utf::disabled())
BOOST_DATA_TEST_CASE_F(
    BS_INSTANCE_MANAGER,
    single_instance_several_alphas,
    (
	bdata::make({ INSTANCE_NAME }) ^
	bdata::make({ INSTANCE_FILENAME }) ^
	bdata::make({ INSTANCE_OPTIMAL_SOLUTION })
    )
    *
    (
//	bdata::make({2, 4}) ^ // alphas
//	bdata::make({3, 6})   // betas
	bdata::make({35}) ^
	bdata::make({70})
    ),
    instance_name, instance_filename, instance_optimal_solution, alpha, beta)
{
  but::unit_test_log.set_threshold_level( but::log_messages );
  setup (
      instance_filename,
      instance_name,
      instance_name + "__" + std::to_string(alpha) + "-" + std::to_string(beta),
      instance_optimal_solution,
      alpha, beta);
}

// =============================================================================
// a single instance family with several pairs (\alpha,\beta)
// =============================================================================

#define INSTANCE_NAMES		   BEASLEY_INSTANCE_NAMES
#define INSTANCE_FILENAMES	   BEASLEY_FILENAMES
#define INSTANCE_OPTIMAL_SOLUTIONS BEASLEY_OPTIMAL_SOLUTIONS

//#define INSTANCE_NAMES	           OKP_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   OKP_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS OKP_OPTIMAL_SOLUTIONS

//#define INSTANCE_NAMES		   HADCHR_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   HADCHR_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS HADCHR_OPTIMAL_SOLUTIONS

//#define INSTANCE_NAMES		   WANG_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   WANG_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS WANG_OPTIMAL_SOLUTIONS

//#define INSTANCE_NAMES		   CGCUT_INSTANCE_NAMES
//#define INSTANCE_FILENAMES	   CGCUT_FILENAMES
//#define INSTANCE_OPTIMAL_SOLUTIONS CGCUT_OPTIMAL_SOLUTIONS

BOOST_TEST_DECORATOR(* utf::disabled())
BOOST_DATA_TEST_CASE_F(
    BS_INSTANCE_MANAGER,
    single_file_several_alphas,
    (
	bdata::make(INSTANCE_NAMES) ^ INSTANCE_FILENAMES ^ INSTANCE_OPTIMAL_SOLUTIONS
    )
    *
    (
//	bdata::make({2, 4}) ^ // alphas
//	bdata::make({3, 6})   // betas
	bdata::make({4}) ^ // alphas
	bdata::make({6})   // betas
    ),
    instance_name, instance_filename, instance_optimal_solution, alpha, beta)
{
  but::unit_test_log.set_threshold_level( but::log_messages );
  setup (
      instance_filename,
      instance_name,
      instance_name + "__" + std::to_string(alpha) + "-" + std::to_string(beta),
      instance_optimal_solution,
      alpha, beta);
}

// =============================================================================
// all 21 instances with several pairs (\alpha,\beta)
// =============================================================================

BOOST_TEST_DECORATOR(* utf::disabled())
BOOST_DATA_TEST_CASE_F(
    BS_INSTANCE_MANAGER,
    all_files_several_alphas,
    (
	(
	    bdata::make(BEASLEY_INSTANCE_NAMES) +
	    OKP_INSTANCE_NAMES +
	    HADCHR_INSTANCE_NAMES +
	    WANG_INSTANCE_NAMES +
	    CGCUT_INSTANCE_NAMES
	)
	^
	(
	    bdata::make(BEASLEY_FILENAMES) +
	    OKP_FILENAMES +
	    HADCHR_FILENAMES +
	    WANG_FILENAMES +
	    CGCUT_FILENAMES
	)
	^
	(
	    bdata::make(BEASLEY_OPTIMAL_SOLUTIONS) +
	    OKP_OPTIMAL_SOLUTIONS +
	    HADCHR_OPTIMAL_SOLUTIONS +
	    WANG_OPTIMAL_SOLUTIONS +
	    CGCUT_OPTIMAL_SOLUTIONS
	)
    )
    *
    (
//    	bdata::make({2, 4}) ^ // alphas
//	bdata::make({3, 6})   // betas
//    	bdata::make({35}) ^
//	bdata::make({70})
	bdata::make({4}) ^
	bdata::make({6})
    ),
    instance_name, instance_filename, instance_optimal_solution, alpha, beta)
{
  but::unit_test_log.set_threshold_level( but::log_messages );
  setup (
      instance_filename,
      instance_name,
      instance_name + "__" + std::to_string(alpha) + "-" + std::to_string(beta),
      instance_optimal_solution,
      alpha, beta);
}

BOOST_AUTO_TEST_SUITE_END() /* beam_search_test */
