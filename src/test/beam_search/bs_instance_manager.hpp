/*!
 * \file bs_instance_manager.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Oct 5, 2018
 */

#ifndef BS_INSTANCE_MANAGER_HPP_
#define BS_INSTANCE_MANAGER_HPP_

#include <fstream>
#include <ostream>
#include <string>

#include <boost/test/included/unit_test.hpp>
#include <boost/date_time/local_time/local_time.hpp>
namespace bpt = boost::posix_time;

#include "../../main/beam_search/beam_search_tree.hpp"
#include "../../main/skp/skp_filter.hpp"
#include "../../main/skp/skp_node.hpp"
using namespace two_dim_skp;

#include "../tools/instance_manager.hpp"
#include "print_visitor.hpp"
#include "empty_visitor.hpp"


namespace two_dim_skp_test
{

  class BS_INSTANCE_MANAGER : public INSTANCE_MANAGER
  {
    typedef SKP_BS_FILTER::LOCAL_EVAL_TYPE LOCAL_EVALUATION;
    typedef SKP_BS_FILTER::GLOBAL_EVAL_TYPE GLOBAL_EVALUATION;
    typedef bs::BEAM_SEARCH_TREE<
	SKP_NODE,
	LOCAL_EVALUATION,
	GLOBAL_EVALUATION> TREE;

  public:

    BS_INSTANCE_MANAGER () : INSTANCE_MANAGER ()
    {
      this->instance_optimal_solution = 0;
      this->alpha = 0;
      this->beta = 0;
    }

    virtual
    ~BS_INSTANCE_MANAGER ()
    {
      //
      // setup beam search filter
      //

      BOOST_TEST_MESSAGE("creating bs filter");
      SKP_BS_FILTER filter (
          std::move(this->prototypes),
	  this->bin_length,
	  this->bin_height,
	  this->alpha,
	  this->beta);

      //
      // executing beam search
      //

      BOOST_TEST_MESSAGE("executing beam search");
      bpt::ptime t1(bpt::microsec_clock::local_time());
      auto result = bs::beam_search(filter);
      bpt::ptime t2(bpt::microsec_clock::local_time());
      bpt::time_duration runtime = t2 - t1;

      //
      // print tree to *.svg
      //

//      PRINT_VISITOR vis(
//	  this->bin_length,
//	  this->bin_height,
//	  result_dir,
//	  this->result_filename);
//      result.depth_first_traversal(vis);

      //
      // print result nodes to *.svg
      //

      // best leaf

      auto& best_leaf = result.get_tree_solution ();
      auto& best_leaf_ge = result.get_tree_global_evaluation ();

      print_bin (result_filename + "__tree",
		 this->bin_length,
		 this->bin_height,
		 best_leaf.get_bin ());
      print_bin (result_filename + "__tree_projected",
		 this->bin_length,
		 this->bin_height,
		 best_leaf_ge.get_bin ());

      // best node

      auto& best_node = result.get_global_solution ();
      auto& best_node_ge = result.get_global_global_evaluation ();

      print_bin (result_filename + "__global",
      		 this->bin_length,
      		 this->bin_height,
      		 best_node.get_bin ());
      print_bin (result_filename + "__global_projected",
		 this->bin_length,
		 this->bin_height,
		 best_node_ge.get_bin ());

      //
      // append result to *.txt file
      //

      // setup output stream

      std::string text_file = result_dir + "results.txt";
      BOOST_TEST_MESSAGE("appending result to txt '" + text_file + "'");
      std::ofstream os (text_file, std::ios::out | std::ios::app);

      // setup time format

      bpt::time_facet *facet = new bpt::time_facet();
      os.imbue(std::locale(std::locale::classic(), facet));
      facet->time_duration_format("%s");

      os << std::boolalpha
	  << this->instance_name << ","
	  << this->instance_optimal_solution << ","
	  << this->alpha << ","
	  << this->beta << ","

	  << best_leaf.get_bin ().size () << ","
	  << best_leaf.get_profit () << ","

	  << best_node.get_bin ().size () << ","
	  << best_node.get_profit () << ","

	  << runtime << ","
	  << result.is_same_branch () << ","
	  << result.is_solution () << ","
	  << result.get_solutions ()

	  << std::endl;
      os.close ();
    }

    void
    setup (const std::string& instance_filename,
	   const std::string& instance_name,
	   const std::string& result_filename,
	   uint instance_optimal_solution, uint alpha, uint beta)
    {
      INSTANCE_MANAGER::setup (instance_filename, instance_name);

      this->result_filename = result_filename;
      this->instance_optimal_solution = instance_optimal_solution;
      this->alpha = alpha;
      this->beta = beta;
    }


    std::string result_filename;
    uint instance_optimal_solution;
    int alpha;
    int beta;
  };

} /* namespace bs */

#endif /* BS_INSTANCE_MANAGER_HPP_ */
