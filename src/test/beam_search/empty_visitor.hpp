/*!
 * \file twodimskptest.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Oct 8, 2018
 */

#ifndef EMPTY_VISITOR_HPP_
#define EMPTY_VISITOR_HPP_

#include "../../main/skp/skp_filter.hpp"
using namespace two_dim_skp;

namespace two_dim_skp_test
{

  class EMPTY_VISITOR
  {
  public:
    void
    visit_node(const SKP_BS_FILTER::NODE_TYPE&,
	       unsigned long,
	       const SKP_BS_FILTER::LOCAL_EVAL_TYPE&,
	       const SKP_BS_FILTER::GLOBAL_EVAL_TYPE&) {}
  };

} /* namespace two_dim_skp_test */

#endif /* EMPTY_VISITOR_HPP_ */
