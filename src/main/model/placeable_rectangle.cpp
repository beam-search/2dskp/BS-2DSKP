/*!
 * \file placeable_rectangle.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Feb 6, 2019
 */

#include "placeable_rectangle.hpp"

namespace two_dim_skp
{
  PLACEABLE_RECTANGLE::PLACEABLE_RECTANGLE (
      uint min_x, uint min_y, uint length, uint height) :
	  RECTANGLE(length, height),
	  min_x (min_x),
	  min_y (min_y),
	  max_x (min_x + length),
	  max_y (min_y + height)
  {
  }

  uint
  PLACEABLE_RECTANGLE::get_min_x () const
  {
    return min_x;
  }

  uint
  PLACEABLE_RECTANGLE::get_min_y () const
  {
    return min_y;
  }

  uint
  PLACEABLE_RECTANGLE::get_max_x () const
  {
    return max_x;
  }

  uint
  PLACEABLE_RECTANGLE::get_max_y () const
  {
    return max_y;
  }

  bool
  PLACEABLE_RECTANGLE::is_intersected (const PLACEABLE_RECTANGLE& r) const
  {
    return r.max_x >= this->min_x &&
	r.min_x <= this->max_x &&
	r.max_y >= this->min_y &&
	r.min_y <= this->max_y;
  }

  bool
  PLACEABLE_RECTANGLE::is_properly_intersected (
      const PLACEABLE_RECTANGLE& r) const
  {
    return r.max_x > this->min_x &&
	r.min_x < this->max_x &&
	r.max_y > this->min_y &&
	r.min_y < this->max_y;
  }

  bool
  PLACEABLE_RECTANGLE::operator== (const PLACEABLE_RECTANGLE& r) const
  {
    return this->min_x == r.min_x &&
	this->min_y == r.min_y &&
	this->max_x == r.max_x &&
	this->max_y == r.max_y;
  }

  bool
  PLACEABLE_RECTANGLE::operator!=(const PLACEABLE_RECTANGLE& r) const
  {
    return !((*this) == r);
  }

} /* namespace two_dim_skp */
