/*!
 * \file rectangle.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jun 19, 2018
 */

#include "rectangle.hpp"

namespace two_dim_skp
{
  RECTANGLE::RECTANGLE (uint length, uint height) :
      length (length),
      height (height),
      area (length * height),
      perimeter (2 * (length + height))
  {
  }

  uint
  RECTANGLE::get_length () const
  {
    return this->length;
  }
  uint
  RECTANGLE::get_height () const
  {
    return this->height;
  }
  uint
  RECTANGLE::get_area () const
  {
    return this->area;
  }
  uint
  RECTANGLE::get_perimeter () const
  {
    return this->perimeter;
  }

  bool
  RECTANGLE::is_square () const
  {
    return this->length == this->height;
  }

  bool
  RECTANGLE::operator>= (const RECTANGLE& r) const
  {
    return this->length >= r.length && this->height >= r.height;
  }

} /* namespace two_dim_skp */
