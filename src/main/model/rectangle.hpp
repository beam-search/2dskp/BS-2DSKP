/*!
 * \file RECTANGLE.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Mar 29, 2018
 */

#ifndef RECTANGLE_HPP_
#define RECTANGLE_HPP_

#include "../types.hpp"

namespace two_dim_skp
{
  class RECTANGLE
  {

  public:

    RECTANGLE (uint length, uint height);

    // default constructible semantics

    RECTANGLE () = default;

    // copy semantics

    RECTANGLE (const RECTANGLE&) = default;
    RECTANGLE&
    operator= (const RECTANGLE&) = default;

    // move semantics

    RECTANGLE (RECTANGLE&& rectangle) = default;
    RECTANGLE&
    operator= (RECTANGLE&&) = default;

    // getters

    uint get_length () const;
    uint get_height () const;
    uint get_area () const;
    uint get_perimeter () const;

    /// Returns true if this RECTANGLE is a square
    ///
    bool
    is_square () const;

    /// Returns true if the given RECTANGLE fits inside this RECTANGLE
    ///
    bool
    operator>= (const RECTANGLE&) const;

  protected:

    uint length;
    uint height;
    uint area;
    uint perimeter;
  };

} /* namespace two_dim_skp */

#endif /* RECTANGLE_HPP_ */
