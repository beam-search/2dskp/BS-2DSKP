/*!
 * \file piece.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Feb 8, 2019
 */

#include "piece.hpp"

namespace two_dim_skp
{

  PIECE::PIECE (uint min_x, uint min_y, const PROTOTYPE& p) :
      PLACEABLE_RECTANGLE (min_x, min_y, p.get_length (), p.get_height ()),
      id (p.get_id ()),
      profit (p.get_profit ())
    {
    }

  PIECE::~PIECE ()
  {
  }

  uint
  PIECE::get_id () const
  {
    return this->id;
  }

  uint
  PIECE::get_profit () const
  {
    return this->profit;
  }

  std::ostream&
  operator<< (std::ostream& out, const PIECE& p)
  {
    out << ", ( " << p.min_x << ", " << p.min_y << " )"
 	<< ", ( " << p.max_x << ", " << p.max_y << " )"
 	<< ", " << p.id
 	<< ", " << p.profit;
    return out;
  }
}
