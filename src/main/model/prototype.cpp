/*!
 * \file Prototype.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 9, 2017
 */

#include <stdexcept>
#include <string>

#include "prototype.hpp"
#include "piece.hpp"

namespace two_dim_skp
{
  //
  // construction and destruction
  //

  PROTOTYPE::PROTOTYPE (uint id, uint length, uint height, uint profit,
			uint max_pieces) :
      RECTANGLE (length, height),
      id (id),
      profit (profit),
      max_pieces (max_pieces),
      efficiency (profit, area),
      cur_pieces (0)
  {
  }

  //Constructor for move
  PROTOTYPE::PROTOTYPE (PROTOTYPE&& proto) :
      RECTANGLE (std::move(proto)),
      id (std::move(proto.id)),
      profit (std::move(proto.profit)),
      max_pieces (std::move(proto.max_pieces)),
      efficiency (std::move(proto.efficiency)),
      cur_pieces (std::move(proto.cur_pieces))
  {
  }

  PROTOTYPE::~PROTOTYPE ()
  {
  }

  uint
  PROTOTYPE::get_id () const
  {
    return this->id;
  }

  uint
  PROTOTYPE::get_max_pieces () const
  {
    return this->max_pieces;
  }

  uint
  PROTOTYPE::get_profit () const
  {
    return this->profit;
  }

  boost::rational<uint>
  PROTOTYPE::get_efficiency() const
  {
    return this->efficiency;
  }
    
  uint
  PROTOTYPE::get_cur_pieces () const
  {
    return this->cur_pieces;
  }

  bool
  PROTOTYPE::has_pieces() const
  {
    return this->cur_pieces < this->max_pieces;
  }

  PIECE
  PROTOTYPE::place (uint min_x, uint min_y)
  {
    if (!has_pieces())
      {
	throw std::runtime_error (
	    "Cannot clone prototype with id " + std::to_string (this->id) + ": "
	    "max number of pieces (" + std::to_string (this->max_pieces) + ") "
	    "reached.");
      }
    this->cur_pieces++;
    return std::move (PIECE (min_x, min_y, *this));
  }

  bool
  PROTOTYPE::operator== (const PROTOTYPE& p) const
  {
    return (this->id == p.id) && (this->cur_pieces == p.cur_pieces);
  }

  bool
  PROTOTYPE::operator< (const PROTOTYPE& p) const
  {
    return this->id == p.id ? this->cur_pieces < p.cur_pieces : this->id < p.id;
  }

  std::ostream&
  operator<< (std::ostream& out, const PROTOTYPE &p)
  {
    out << "id = " << p.id
	<< ", width = " << p.length
	<< ", height = " << p.height
	<< ", profit = " << p.profit
	<< ", max copies = " << p.max_pieces
	<< ", cur copies = " << p.cur_pieces;
    return out;
  }

} /* namespace two_dim_skp */
