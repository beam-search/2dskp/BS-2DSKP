/*!
 * \file grasp_maximal_rectangle.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Feb 21, 2018
 */

#include "maximal_rectangle.hpp"

namespace two_dim_skp
{
  MAXIMAL_RECTANGLE::MAXIMAL_RECTANGLE (uint min_x, uint min_y, uint length,
					uint height,
					std::initializer_list<uint> top,
					std::initializer_list<uint> bottom,
					std::initializer_list<uint> left,
					std::initializer_list<uint> right) :
      PLACEABLE_RECTANGLE (min_x, min_y, length, height),
      adjacent_perimeter (0),
      top (top),
      bottom (bottom),
      left (left),
      right (right)
  {
  }

  uint
  MAXIMAL_RECTANGLE::get_adjacent_perimeter () const
  {
    return this->adjacent_perimeter;
  }

  std::vector<uint>&
  MAXIMAL_RECTANGLE::get_top ()
  {
    return this->top;
  }

  const std::vector<uint>&
  MAXIMAL_RECTANGLE::get_top () const
  {
    return this->top;
  }

  void
  MAXIMAL_RECTANGLE::update_top (uint bottom, uint top)
  {
    update_intervals(this->top, bottom, top);
  }

  std::vector<uint>&
  MAXIMAL_RECTANGLE::get_bottom ()
  {
    return this->bottom;
  }

  const std::vector<uint>&
  MAXIMAL_RECTANGLE::get_bottom () const
  {
    return this->bottom;
  }

  void
  MAXIMAL_RECTANGLE::update_bottom (uint bottom, uint top)
  {
    update_intervals(this->bottom, bottom, top);
  }

  std::vector<uint>&
  MAXIMAL_RECTANGLE::get_left()
  {
    return this->left;
  }

  const std::vector<uint>&
  MAXIMAL_RECTANGLE::get_left () const
  {
    return this->left;
  }

  void
  MAXIMAL_RECTANGLE::update_left (uint bottom, uint top)
  {
    update_intervals(this->left, bottom, top);
  }

  std::vector<uint>&
  MAXIMAL_RECTANGLE::get_right ()
  {
    return this->right;
  }

  const std::vector<uint>&
  MAXIMAL_RECTANGLE::get_right () const
  {
    return this->right;
  }

  void
  MAXIMAL_RECTANGLE::update_right (uint bottom, uint top)
  {
    update_intervals(this->right, bottom, top);
  }

  MAXIMAL_RECTANGLE&
  MAXIMAL_RECTANGLE::update ()
  {
    this->adjacent_perimeter =
	sum_intervals (this->top.begin(), this->top.end())
	+ sum_intervals (this->bottom.begin(), this->bottom.end())
	+ sum_intervals (this->left.begin(), this->left.end())
	+ sum_intervals (this->right.begin(), this->right.end());
    return *this;
  }

  void
  MAXIMAL_RECTANGLE::subtract (const MAXIMAL_RECTANGLE& r)
  {
    // TODO Update intervals properly

    // if both MAXIMAL_RECTANGLEs are equal or are not properly intersected,
    // return an empty MAXIMAL_RECTANGLE

    if ((*this) == r || !this->is_properly_intersected (r))
      {
	return;
      }

    // Compute the the intersection. The intersection between MAXIMAL_RECTANGLEs
    // is a rectangle. Moreover, a MAXIMAL_RECTANGLE cannot properly contain an
    // edge of another MAXIMAL_RECTANGLE.

    this->min_x = std::max(this->min_x, r.min_x);
    this->min_y = std::max(this->min_y, r.min_y);
    this->length = std::min(this->length, r.length);
    this->height = std::min(this->height, r.height);
    this->max_x = this->min_x + this->length;
    this->max_y = this->min_y + this->height;
  }

} /* namespace two_dim_skp */
