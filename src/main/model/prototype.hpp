/*!
 * \file Prototype.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 8, 2017
 */

#ifndef PROTOTYPE_HPP_
#define PROTOTYPE_HPP_

#include <iostream>
#include <ostream>

#include <boost/rational.hpp>

#include "../types.hpp"
#include "rectangle.hpp"

namespace two_dim_skp
{
  /// A piece prototype
  ///
  class PROTOTYPE : public RECTANGLE
  {
    friend std::ostream& operator<<( std::ostream &, const PROTOTYPE & );

  public:

    PROTOTYPE (uint id, uint length, uint height, uint profit, uint max_pieces);

    virtual
    ~PROTOTYPE (); //Destructor

    // default constructible semantics

    PROTOTYPE () = default;

    // copy semantics

    PROTOTYPE (const PROTOTYPE&) = default;
    PROTOTYPE&
    operator= (const PROTOTYPE&) = default;

    // move semantics

    PROTOTYPE (PROTOTYPE&&);
    PROTOTYPE&
    operator= (PROTOTYPE&&) = default;

    // methods

    uint get_id () const;
    uint get_max_pieces () const;
    uint get_profit () const;
    uint get_cur_pieces () const;
    boost::rational<uint> get_efficiency () const;

    /// Returns true if the prototype has more pieces to clone
    ///
    bool
    has_pieces () const;

    /// Creates a Piece from this PROTOTYPE
    ///
    class PIECE
    place (uint min_x, uint min_y);

    // operators

    /// Returns true if both prototypes have the same id
    ///
    bool
    operator== (const PROTOTYPE& p) const;

    /// Lexicographically compares the prototypes by id and number of placed
    /// pieces
    ///
    bool
    operator< (const PROTOTYPE& p) const;

  private:

    /// A unique identifier for the prototype.
    ///
    uint id;

    /// The profit one obtain when correctly placing a piece created from
    /// the PROTOTYPE
    ///
    uint profit;

    /// The maximum number of pieces that can be created from the PROTOTYPE
    ///
    uint max_pieces;

    /// The efficiency of a piece: profit / area
    ///
    /// Efficiency is stored as a rational number to improve precision in
    /// comparisons.
    ///
    boost::rational<uint> efficiency;

    // The number of pieces cloned from this prototype
    //
    uint cur_pieces;
  };

} /* namespace two_dim_skp */

#endif /* PROTOTYPE_HPP_ */
