/*!
 * \file placeable_rectangle.h
 *
 * \author Carlos Alegría Galicia
 * \date   Mar 29, 2018
 */

#ifndef PLACEABLE_RECTANGLE_HPP_
#define PLACEABLE_RECTANGLE_HPP_

#include "../types.hpp"
#include "rectangle.hpp"

namespace two_dim_skp
{

  /// A rectangle that can be located in the plane

  class PLACEABLE_RECTANGLE : public RECTANGLE
  {
  public:
    PLACEABLE_RECTANGLE (uint min_x, uint min_y, uint length, uint height);

    // default constructible semantics

    PLACEABLE_RECTANGLE () = default;

    // copy semantics

    PLACEABLE_RECTANGLE (const PLACEABLE_RECTANGLE&) = default;
    PLACEABLE_RECTANGLE&
    operator= (const PLACEABLE_RECTANGLE&) = default;

    // move semantics

    PLACEABLE_RECTANGLE (PLACEABLE_RECTANGLE&&) = default;
    PLACEABLE_RECTANGLE&
    operator= (PLACEABLE_RECTANGLE&&) = default;

    // operations

    uint get_min_x () const;
    uint get_min_y () const;
    uint get_max_x () const;
    uint get_max_y () const;

    /// Return true if this object is intersected by the given
    /// PLACEABLE_RECTANGLE.
    ///
    /// The test considers as a valid intersection the case where the
    /// intersection between a pair of edges of the rectangles contains more
    /// than one point.
    ///
    bool is_intersected (const PLACEABLE_RECTANGLE&) const;

    /// Return true if this object is intersected by the given
    /// PLACEABLE_RECTANGLE.
    ///
    /// The test does not considers as a valid intersection the case where the
    /// intersection between a pair of edges of the rectangles contains more
    /// than one point.
    ///
    bool is_properly_intersected (const PLACEABLE_RECTANGLE&) const;

    /// Return true if the coordinates of the corners of this object are equal
    /// to the coordinates of the corners of the given PLACEABLE_RECTANGLE.
    ///
    bool
    operator==(const PLACEABLE_RECTANGLE&) const;
    bool
    operator!=(const PLACEABLE_RECTANGLE&) const;

  protected:

    uint min_x;
    uint min_y;
    uint max_x;
    uint max_y;
  };

} /* namespace two_dim_skp */

#endif /* PLACEABLE_RECTANGLE_HPP_ */
