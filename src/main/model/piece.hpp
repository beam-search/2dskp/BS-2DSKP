/*!
 * \file Piece.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Oct 31, 2017
 */

#ifndef PIECE_HPP_
#define PIECE_HPP_

#include <limits>
#include <iostream>

#include "../types.hpp"
#include "prototype.hpp"
#include "placeable_rectangle.hpp"

namespace two_dim_skp
{

  /// Piece class
  ///
  /// A PROTOTYPE that can be located in the plane.
  ///
  class PIECE : public PLACEABLE_RECTANGLE
  {
    friend std::ostream& operator<<( std::ostream &, const PIECE & );

  public:

    PIECE (uint min_x, uint min_y, const PROTOTYPE& p);

    //Comentar para ver si se necesita este constructor. Sólo toma el prototipo y lo coloca en (0,0).
    //Checar si se usa.
//    PIECE (const PROTOTYPE &proto) :
//	PIECE (proto,
//	       std::numeric_limits<uint>::min (),
//	       std::numeric_limits<uint>::min ())
//    {
//    }

    virtual
    ~PIECE ();

    //
    // copy semantics
    //

    PIECE (const PIECE &) = default;
    PIECE&
    operator= (const PIECE&) = default;

    //
    // move semantics
    //

    PIECE (PIECE &&) = default;
    PIECE&
    operator= (PIECE&&) = default;

    //
    // operators
    //

    uint get_id () const;
    uint get_profit () const;

  private:

    /// The identifier of the prototype that placed this PIECE
    ///
    uint id;

    /// The profit of the prototype that placed this PIECE
    ///
    uint profit;

  };

} /* two_dim_skp */

#endif /* PIECE_HPP_ */
