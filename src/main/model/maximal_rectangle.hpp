/*!
 * \file grasp_maximal_rectangle.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Feb 21, 2018
 */

#ifndef MAXIMAL_RECTANGLE_HPP_
#define MAXIMAL_RECTANGLE_HPP_

#include <vector>
#include <iterator>
#include <algorithm>
#include <initializer_list>

#include "../types.hpp"
#include "rectangle.hpp"
#include "placeable_rectangle.hpp"

namespace two_dim_skp
{
  //
  // Maximal rectangle
  //
  // A rectangle within the stock rectangle that shares each of its sides with
  // at least one neighbor piece. Each neighbor piece is represented as an
  // interval. The set of neighbor pieces is represented by a sorted set of
  // intervals (see the figure for reference):
  //
  // - From left to right by left point x-coordinate
  // - From bottom to top by bottom point y-coordinate
  //
  //     x------------->>-------------x
  //     |                            |
  //     ^                            ^
  //     ^                            ^
  //     |                            |
  //     x------------->>-------------x
  //
  class MAXIMAL_RECTANGLE : public PLACEABLE_RECTANGLE
  {
  public:

    MAXIMAL_RECTANGLE (uint min_x, uint min_y, uint length, uint height,
		       std::initializer_list<uint> top = {},
		       std::initializer_list<uint> bottom = {},
		       std::initializer_list<uint> left = {},
		       std::initializer_list<uint> right = {});
        
    // default constructible semantics
        
    MAXIMAL_RECTANGLE () = default;
        
    // copy semantics
        
    MAXIMAL_RECTANGLE (const MAXIMAL_RECTANGLE&) = default;
    MAXIMAL_RECTANGLE&
    operator= (const MAXIMAL_RECTANGLE&) = default;
        
    // move semantics
        
    MAXIMAL_RECTANGLE (MAXIMAL_RECTANGLE&&) = default;
    MAXIMAL_RECTANGLE&
    operator= (MAXIMAL_RECTANGLE&&) = default;
        
    // operations
        
    uint
    get_adjacent_perimeter () const;
        
    // intervals operations

    std::vector<uint>&
    get_top();

    const std::vector<uint>&
    get_top () const;
        
    void
    update_top (uint bottom, uint top);

    std::vector<uint>&
    get_bottom();

    const std::vector<uint>&
    get_bottom() const;

    void
    update_bottom (uint bottom, uint top);

    std::vector<uint>&
    get_left();

    const std::vector<uint>&
    get_left () const;

    void
    update_left (uint bottom, uint top);

    std::vector<uint>&
    get_right ();

    const std::vector<uint>&
    get_right () const;

    void
    update_right (uint bottom, uint top);

    // update adjacent perimeter

    MAXIMAL_RECTANGLE&
    update ();

    /// Returns the rest between this and the given MAXIMAL_RECTANGLE
    ///
    void
    subtract (const MAXIMAL_RECTANGLE&);

  private:

    // properties

    uint adjacent_perimeter;

    std::vector<uint> top;
    std::vector<uint> bottom;
    std::vector<uint> left;
    std::vector<uint> right;
        
    // update existing intervals according to the given interval
        
    template<template<typename, typename ...> class Container>
    void
    update_intervals(Container<uint>& intervals, uint ini, uint fin)
    {
      auto begin = intervals.begin();
      auto lower = lower_bound (begin, intervals.end(), ini);//Encuentra el último elemento más pequeño que ini
      auto upper = upper_bound (begin, intervals.end(), fin);//Encuentra el primer elemento más grande que fin

      bool insert_lower = ((lower - begin) + 1) % 2 != 0;
      bool insert_upper = ((upper - begin) + 1) % 2 != 0;
            
      auto it = intervals.erase(lower, upper);
      if (insert_upper) it = intervals.insert(it, fin);
      if (insert_lower) intervals.insert(it, ini);
    }
        
    // sum the length of the intervals of the given interval list

    template<class InputIt>
    typename std::iterator_traits<InputIt>::value_type
    sum_intervals (InputIt first, InputIt last)
    {
      if (first == last--) return 0;
      typename std::iterator_traits<InputIt>::value_type sum = 0;
      while (first != last)
	{
	  sum += *(last) - *(--last);
	}
      return sum;
    }
  };

} /* namespace two_dim_skp */

#endif /* MAXIMAL_RECTANGLE_HPP_ */
