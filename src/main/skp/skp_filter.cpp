/*!
 * \file SKPBSFILTER.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   May 2, 2018
 */

#include "skp_filter.hpp"

#include <deque>
#include <iterator>
#include <algorithm>
#include <unordered_map>

#include "skp_functions.hpp"
#include "skp_evaluations.hpp"
#include "../algorithms/best_fit.hpp"
#include "../algorithms/place_piece.hpp"

namespace two_dim_skp
{

  SKP_BS_FILTER::SKP_BS_FILTER (
      PROTOTYPES&& prototypes,
      uint bin_length, uint bin_height,
      uint filter_width, uint beam_width,
      POLICY local_eval_policy,
      POLICY global_eval_policy) :
	  bin_length (bin_length),
	  bin_height (bin_height),
	  filter_width (filter_width),
	  beam_width (beam_width)
  {
    // the (non-mutable) root node

    root = SKP_NODE (
	// an empty set of placed pieces
	{ },

	// the given set of prototypes
	prototypes,

	// the cloned prototype is arbitrarily chosen from the given set of
	// prototypes
	prototypes.begin ()->first,

	// a single maximal rectangle with the same size of the bin
	std::move (create_maximal_rectangles (bin_length, bin_height)));

    switch (local_eval_policy) {
      case AREA:
	local_eval_comp = increasing_area;
	break;
      case EFFICIENCY:
	local_eval_comp = increasing_efficiency;
	break;
      case PROFIT:
	local_eval_comp = increasing_profit;
	break;
    }

    switch (global_eval_policy) {
      case AREA:
	global_eval_comp = increasing_area;
	break;
      case EFFICIENCY:
	global_eval_comp = increasing_efficiency;
	break;
      case PROFIT:
	global_eval_comp = increasing_profit;
	break;
    }
  }

  SKP_BS_FILTER::~SKP_BS_FILTER ()
  {
  }

  // TODO We create one candidate children per prototype. When there are more
  // than 50 prototypes, chose a subset (randomly?, sorting the set?) of
  // candidates of constant cardinality.

  void
  SKP_BS_FILTER::create_nodes (NODES& nodes)
  {
    // A vector with the parent nodes that can have children (they have at least
    // one piece to place)

    std::vector<NODES::size_type> parents;

    {
#ifdef SET_DIFFERENCE
      // use the prototypes vector evaluation
      typedef std::vector <
	  std::pair <NODES::size_type, std::vector< PROTOTYPE * > > > EVAL_NODES;
#else
      // use the node's local evaluation
      typedef std::vector <
	  std::pair <NODES::size_type, LOCAL_EVAL_TYPE > > EVAL_NODES;
#endif

      // Evaluate parent nodes. We only consider nodes that can have children
      // (those with at least one PROTOTYPE with a PIECE that can be placed in
      // the bin)

      EVAL_NODES eval_nodes;
      for (NODES::size_type i = 0, n = nodes.size (); i < n; ++i)
	{
	  auto& maximal_rectangles = nodes[i].first->get_maximal_rectangles ();
	  auto& prototypes = nodes[i].first->get_prototypes ();

	  if (std::find_if(
	      prototypes.begin (),
	      prototypes.end (),
	      [&maximal_rectangles] (auto& el)
	      {
		return can_place_piece (maximal_rectangles, el.second);
	      }) !=  prototypes.end ())
	    {
#ifdef SET_DIFFERENCE
	      eval_nodes.emplace_back (
		  i,
		  std::move(efficiency_vector (*(i->first))));
#else
	      eval_nodes.emplace_back (i, local_evaluation (*(nodes[i].first)));
#endif
	    }
	}

      // Sort the nodes in processing order

#ifdef SET_DIFFERENCE

      // for each evaluated node, sort the evaluation prototypes in increasing
      // order according to their id and number of pieces

      for (auto& eval_node : eval_nodes)
	{
	  auto& vect = std::get<2> (eval_node);
	  std::sort(vect.begin (), vect.end (),
		    [] (auto& a, auto& b) { return (*a) < (*b); });
	}

      // sort all evaluation vector using the symmetric difference of the fit
      // prototypes evaluation vector

      std::sort(
	  eval_nodes.begin (),
	  eval_nodes.end (),
	  [] (auto& a, auto& b)
	  {
	    return !compare_efficiency_vectors (a.second, b.second);
	  });
#else
      // they are already sorted by local evaluation
//      std::sort(eval_nodes.begin (),
//		eval_nodes.end (),
//		[] (auto& a, auto& b) { return compare (a.second, b.second); });
#endif

      // fill sorted nodes

      std::transform(
	  eval_nodes.begin(),
	  eval_nodes.end(),
	  std::back_inserter(parents),
	  [] (auto& el) { return el.first; });
    }

    // creating children

    for (auto i : parents)
      {
	auto& father = nodes[i].first;
	std::vector<std::pair <NODE_TYPE, LOCAL_EVAL_TYPE> > eval_children;

	auto& father_bin = father->get_bin ();
	auto& father_prototypes = father->get_prototypes ();
	auto& father_maximal_rectangles = father->get_maximal_rectangles ();

	// removing the prototypes that have no more pieces to place

	std::vector<PROTOTYPES::key_type> pr_indexes;
	std::for_each(
	    father_prototypes.begin (),
	    father_prototypes.end (),
	    [&pr_indexes, &father_maximal_rectangles] (auto& el)
	    {
	      if (can_place_piece (father_maximal_rectangles, el.second))
		{
		  pr_indexes.push_back (el.first);
		}
	    });

	for (auto& father_index : pr_indexes)
	  {
	    // create all possible children

	    std::vector<NODE_TYPE> new_children;
	    {
	      std::vector<PROTOTYPES> new_prototypes;
	      std::vector<std::vector<MAXIMAL_RECTANGLE> > new_rectangles;
	      std::vector<PIECE> new_pieces;

	      place_pieces (father_maximal_rectangles,
			    father_prototypes,
			    father_index,
			    std::back_inserter (new_prototypes),
			    std::back_inserter (new_rectangles),
			    std::back_inserter (new_pieces));

	      auto pr_it = new_prototypes.begin ();
	      auto r_it = new_rectangles.begin ();
	      for (auto& piece : new_pieces)
		{
		  auto new_bin = father_bin;
		  new_bin.push_back (std::move (piece));

		  SKP_BS_FILTER::NODE_TYPE child (std::move (new_bin),
						  std::move (*(pr_it++)),
						  father_index,
						  std::move (*(r_it++)));

		  new_children.push_back (std::move (child));
		}
	    }

	    // add and locally evaluate children that are not symmetric

	    for (auto& child : new_children)
	      {
		// test if node is symmetric
		// TODO traverse the array upto this point (do not use indexes)

		bool is_symmetric = false;
		for (NODES::size_type s_i = 0; s_i < i; ++s_i)
		  {
		    auto& s_children_vector = nodes[s_i].second;
		    auto ptr = std::find_if (s_children_vector.begin (),
					     s_children_vector.end (),
					     [&child] (auto& node)
					     {
					      return are_simmetric(child, node);
					     });
		    if (ptr != s_children_vector.end ())
		      {
			is_symmetric = true;
			break;
		      }
		  }

		// Push node to output if it is not symmetric

		if (!is_symmetric)
		  {
		    auto eval = local_evaluation(child);
		    eval_children.emplace_back(std::move (child),
					       std::move (eval));
		  }
	      }
	  }

	// sort the children by local evaluation

	std::sort(eval_children.begin(), eval_children.end(),
		  [this] (auto& a, auto& b)
		  {
		    return compare(a.second, b.second);
		  });

	// write to output at most \alpha nodes

	auto first = eval_children.begin ();
	std::advance (
	    first,
	    eval_children.size () - std::min (eval_children.size (), (size_t) this->filter_width));
	std::transform (first, eval_children.end (),
			std::back_inserter (nodes[i].second),
			[] (auto& e) { return std::move (e.first); });
      }
  }

  SKP_BS_FILTER::LOCAL_EVAL_TYPE
  SKP_BS_FILTER::local_evaluation (const NODE_TYPE& node)
  {
    auto& maximal_rectangles = node.get_maximal_rectangles ();

    // the efficiency

    auto efficiency = node.get_cloned_prototype ().get_efficiency ();

    // the sorted vector of maximal rectangles

    std::vector<PLACEABLE_RECTANGLE> sorted_maximals;
    std::transform(
	maximal_rectangles.begin(),
	maximal_rectangles.end(),
	std::back_inserter (sorted_maximals),
	[] (auto& r) { return (PLACEABLE_RECTANGLE &) r; });
    std::sort (sorted_maximals.begin (), sorted_maximals.end (), decreasing_area);

    // the sorted vector of lowest maximal rectangles

    std::vector<PLACEABLE_RECTANGLE> lowest_maximals;

    uint lowest_coord = std::min_element(
	maximal_rectangles.begin (),
	maximal_rectangles.end (),
	increasing_min_y)->get_min_y ();

    std::for_each(
	maximal_rectangles.begin (),
	maximal_rectangles.end (),
	[&lowest_coord, &lowest_maximals] (const auto& r)
	{
	  if (r.get_min_y () == lowest_coord)
	    {
	      lowest_maximals.push_back(r);
	    }
	});

    std::sort(lowest_maximals.begin (), lowest_maximals.end (), decreasing_area);

    // the real and potential wastes

    uint real = real_waste(node);
    uint potential = potential_waste(node);

    // returning the tuple

    return std::move (std::make_tuple (std::move (efficiency),
				       std::move (sorted_maximals),
				       std::move (lowest_maximals),
				       std::move (real),
				       std::move (potential)));
  }

  bool
  SKP_BS_FILTER::compare (const LOCAL_EVAL_TYPE& a, const LOCAL_EVAL_TYPE& b)
  {
    // efficiency

    {
      auto& a_efficiency = std::get<0> (a);
      auto& b_efficiency = std::get<0> (b);

      if (a_efficiency != b_efficiency)
	{
	  return a_efficiency < b_efficiency;
	}
    }

    // if efficiencies are the same, compare the sorted vector of
    // MAXIMAL_RECTANGLEs

    {
      auto& a_mr = std::get<1> (a);
      auto& b_mr = std::get<1> (b);

      if (!std::equal(
	  a_mr.begin (), a_mr.end (),
	  b_mr.begin (), b_mr.end (),
	  [] (auto& r_a, auto& r_b) { return r_a.get_area () == r_b.get_area (); }))
	{
	  return std::lexicographical_compare (a_mr.begin (), a_mr.end (),
					       b_mr.begin (), b_mr.end (),
					       increasing_area);
	}
    }

    // if the sorted vector of MAXIMAL_RECTANGLEs are equal, compare the sorted
    // vector of lowest MAXIMAL_RECTANGLEs

    {
      auto& a_lowest_mr = std::get<2> (a);
      auto& b_lowest_mr = std::get<2> (b);

      if (!std::equal(
	  a_lowest_mr.begin (), a_lowest_mr.end (),
	  b_lowest_mr.begin (), b_lowest_mr.end (),
	  [] (auto& r_a, auto& r_b) { return r_a.get_area () == r_b.get_area (); }))
	{
	  return std::lexicographical_compare (
	      a_lowest_mr.begin (), a_lowest_mr.end (),
	      b_lowest_mr.begin (), b_lowest_mr.end (),
	      increasing_area);
	}
    }

    // if the sorted vector of lowest MAXIMAL_RECTANGLEs are equal, compare the
    // real waste

    {
      auto& a_real_waste = std::get<3> (a);
      auto& b_real_waste = std::get<3> (b);

      if (a_real_waste != b_real_waste)
      	{
      	  return a_real_waste < b_real_waste;
      	}
    }

    // finally, if real wastes are the same, compare the potential waste

    auto& a_eval = std::get<4> (a);
    auto& b_eval = std::get<4> (b);

    return a_eval < b_eval;
  }

  SKP_BS_FILTER::GLOBAL_EVAL_TYPE
  SKP_BS_FILTER::global_evaluation (const NODE_TYPE& node)
  {
#ifdef GE_MIN_KNAP
    return profit_knapsack (node);
#else
    return std::move (best_fit (node, this->global_eval_comp));
#endif
  }

  bool
  SKP_BS_FILTER::compare (const GLOBAL_EVAL_TYPE& a, const GLOBAL_EVAL_TYPE& b)
  {
#ifdef GE_MIN_KNAP
    return a < b;
#else
    return a.get_profit () < b.get_profit ();
#endif
  }

} /* namespace two_dim_skp */
