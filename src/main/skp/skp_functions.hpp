/*!
 * \file skp_layer_functions.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Dec 17, 2018
 */

#ifndef SKP_FUNCTIONS_HPP_
#define SKP_FUNCTIONS_HPP_

#include "../model/prototype.hpp"
#include "../skp/skp_filter.hpp"

#include <vector>
#include <iterator>

namespace two_dim_skp
{
  // Returns true if the nodes are symmetric
  //
  // A pair of nodes are symmetric if their set of placed pieces is equal, their
  // set of maximal rectangles have the same cardinality and their elements can
  // be paired in a perfect matching such that the maximal rectangles of every
  // matched pair have equal length and height.

  bool
  are_simmetric (const SKP_BS_FILTER::NODE_TYPE& a,
		 const SKP_BS_FILTER::NODE_TYPE& b);

  // Compares the given RECTANGLEs by area

  bool
  increasing_area (const RECTANGLE& a, const RECTANGLE& b);

  bool
  decreasing_area (const RECTANGLE& a, const RECTANGLE& b);

  // Compare the given PLACEABLE_RECTANGLEs by min_y coordinate

  bool
  increasing_min_y (const PLACEABLE_RECTANGLE& a, const PLACEABLE_RECTANGLE& b);

  // Compare the given PLACEABLE_RECTANGLEs by area, solving ties with the
  // min_y coordinate

  bool
  decreasing_area__increasing_min_y (const PLACEABLE_RECTANGLE& a, const PLACEABLE_RECTANGLE& b);

  // Compares the given PROTOTYPEs by profit

  bool
  increasing_profit (const PROTOTYPE& a, const PROTOTYPE& b);

  // Compares the given PROTOTYPEs by efficiency

  bool
  increasing_efficiency (const PROTOTYPE& a, const PROTOTYPE& b);
}

#endif /* SKP_FUNCTIONS_HPP_ */
