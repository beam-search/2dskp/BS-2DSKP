/*!
 * \file skp_functions.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Dec 17, 2018
 */

#include "../skp/skp_functions.hpp"

#include <algorithm>
#include <vector>

#include "../algorithms/place_piece.hpp"
#include "../model/placeable_rectangle.hpp"

namespace two_dim_skp
{
  bool
  are_simmetric (const SKP_BS_FILTER::NODE_TYPE& node_a,
		 const SKP_BS_FILTER::NODE_TYPE& node_b)
  {
    // check if both nodes have the same pieces placed

    const auto& bin = node_b.get_bin();

    std::vector<size_t> indexes (bin.size ());
    std::iota (indexes.begin (), indexes.end (), 0);

    for (const auto& a_piece : node_a.get_bin ())
      {
    	auto it = std::find_if (
    	    indexes.begin (), indexes.end (),
    	    [&a_piece,bin] (size_t index)
    	    {
    	      return a_piece.get_id () == bin[index].get_id ();
    	    });
    	if (it == indexes.end ())
    	  {
    	    return false;
    	  }
    	indexes.erase(it);
      }

    // check if the nodes have maximal rectangles with the same dimensions
    // TODO Change to a more efficient algorithm. Current consumes O(n²) time

    const auto& rectangles = node_b.get_maximal_rectangles ();

    indexes.clear ();
    indexes.resize (rectangles.size ());
    std::iota (indexes.begin (), indexes.end (), 0);

    for (const auto& a_rectangle : node_a.get_maximal_rectangles ())
      {
	auto it = std::find_if (
	    indexes.begin (), indexes.end (),
	    [&a_rectangle,rectangles] (size_t index)
	    {
	      return (a_rectangle.get_length () == rectangles[index].get_length ())
		  && (a_rectangle.get_height () == rectangles[index].get_height ());
	    });
	if (it == indexes.end ())
	  {
	    return false;
	  }
	indexes.erase(it);
      }

    return true;
  }

  bool
  increasing_area (const RECTANGLE& a, const RECTANGLE& b)
  {
    return a.get_area () < b.get_area ();
  }

  bool
  decreasing_area (const RECTANGLE& a, const RECTANGLE& b)
  {
    return a.get_area () > b.get_area ();
  }

  bool
  increasing_min_y (const PLACEABLE_RECTANGLE& a, const PLACEABLE_RECTANGLE& b)
  {
    return a.get_max_y () < b.get_min_y ();

  }

  bool
  decreasing_area__increasing_min_y (const PLACEABLE_RECTANGLE& a, const PLACEABLE_RECTANGLE& b)
  {
    uint a_area = a.get_area ();
    uint b_area = b.get_area ();

    return (a_area > b_area) || ((a_area == b_area) && (a.get_min_y () < b.get_min_y ()));
  }



  bool
  increasing_profit(const PROTOTYPE& a, const PROTOTYPE& b)
  {
    return a.get_profit () < b.get_profit ();
  }

  bool
  increasing_efficiency(const PROTOTYPE& a, const PROTOTYPE& b)
  {
    return a.get_profit () * b.get_area () < a.get_area () * b.get_profit ();
  }
}
