/*!
 * \file SKPBSFILTER.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   May 2, 2018
 */

#ifndef SKP_BS_FILTER_HPP_
#define SKP_BS_FILTER_HPP_

#include <boost/rational.hpp>
#include <functional>
#include <utility>
#include <vector>

#include "../model/prototype.hpp"
#include "../skp/skp_node.hpp"
#include "../types.hpp"

namespace two_dim_skp
{

  class SKP_BS_FILTER
  {
  public:

    //
    // Types required by the beam search interface
    //

    // the type of the node inserted in the beam search tree

    typedef SKP_NODE NODE_TYPE;

    // the type of the global evaluation

#ifdef GE_MIN_KNAP
    typedef long GLOBAL_EVAL_TYPE;
#else
    typedef NODE_TYPE GLOBAL_EVAL_TYPE;
#endif

    // the type of the local evaluation

    typedef std::tuple<
	boost::rational<uint>,
	std::vector<PLACEABLE_RECTANGLE>,
	std::vector<PLACEABLE_RECTANGLE>, uint, uint> LOCAL_EVAL_TYPE;

    //
    // Auxiliary types
    //

    // the vector where the created nodes are stored

    typedef std::vector< std::pair< NODE_TYPE *, std::vector<NODE_TYPE> > > NODES;

    // the policy used to sort pieces in global and local evaluations

    enum POLICY { PROFIT, AREA, EFFICIENCY };

    //
    // Construction and destruction
    //

    SKP_BS_FILTER (
	PROTOTYPES&& prototypes,
	uint length, uint height,
	uint filter_width = 2, uint beam_width = 3,
	POLICY local_eval_policy = POLICY::EFFICIENCY,
	POLICY global_eval_policy = POLICY::EFFICIENCY);

    virtual
    ~SKP_BS_FILTER ();

    //
    // Methods required by the beam search interface
    //

    // returns the parameter indicating the maximum number of children per node
    // in the beam search tree

    uint
    get_filter_width() const { return this->filter_width; }

    // returns the parameter indicating the maximum number of branches that are
    // to be explored per level in the beam search tree

    uint
    get_beam_width() const { return this->beam_width; }

    // returns the non-mutable root node of the beam search tree

    const NODE_TYPE&
    get_root_node() const { return this->root; }

    // creates the set of children per parent in the given parents vector

    void
    create_nodes (NODES& nodes);

    // returns the local evaluation of the given node
    //
    // the evaluation is given by the efficiency of piece that was placed to
    // this node

    LOCAL_EVAL_TYPE
    local_evaluation (const NODE_TYPE& node);

    bool
    compare (const LOCAL_EVAL_TYPE& a, const LOCAL_EVAL_TYPE& b);

    // returns the global evaluation of the given node
    //
    // the evaluation is expressed as a pair <NODE, EVAL>, where NODE is the
    // node obtained after projecting the given node, and EVAL is the object of
    // type GLOBAL_EVAL_TYPE representing the global evaluation of the given
    // node
    //
    // the projection node is obtained by using the modified best fit algorithm
    // (the implementation can be found in the best_fit*.* files)
    //
    // the global evaluation is given by the profit of the projected node

    GLOBAL_EVAL_TYPE
    global_evaluation (const NODE_TYPE& node);

    // returns true if "a" is less than "b"

    bool
    compare (const GLOBAL_EVAL_TYPE& a, const GLOBAL_EVAL_TYPE& b);

  private:

    //
    // properties
    //

    const uint bin_length;
    const uint bin_height;
    const uint filter_width;
    const uint beam_width;

    NODE_TYPE root;

    std::function<bool(const PROTOTYPE&, const PROTOTYPE&)> local_eval_comp;
    std::function<bool(const PROTOTYPE&, const PROTOTYPE&)> global_eval_comp;
  };

} /* namespace two_dim_skp */

#endif /* SKP_BS_FILTER_HPP_ */
