/*!
 * \file node_evaluations.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jan 28, 2019
 */

#ifndef NODE_EVALUATIONS_HPP_
#define NODE_EVALUATIONS_HPP_

#include <vector>
#include <utility>
#include <functional>

#include "skp_node.hpp"

namespace two_dim_skp
{
  // Evaluates the given SKP_NODE with a vector of prototypes
  //
  // The vector contains the subset of prototypes that fit into the same maximal
  // rectangle, that has the greatest accumulated sum of efficiency. If there
  // are two vectors with the same accumulated efficiency, the vector is the one
  // corresponding to the maximal rectangle with greatest area.

  std::vector<const PROTOTYPE *>
  efficiency_vector (const SKP_NODE& node);

  bool
  compare_efficiency_vectors (const std::vector<PROTOTYPE *>& a,
			      const std::vector<PROTOTYPE *>& b);

  // Evaluates the given SKP_NODE with the maximum profit that can be achieved
  // with the available pieces.
  //
  // The maximum profit is achieved by solving the 0-1 knapsack problem
  //
  //   maximize   \sum_{i=1}^{n} p_{i} x_{i}
  //   subject to \sum_{i=1}^{n} w_{i} x_{i} \leq A
  //              x_{i} \in \{0,1\}, i = 1,\ldots,n
  //
  // where the set of objects is given by A is the area available of the bin,
  // p_i is the profit and w_i is the area of the ith available piece. More
  // information about the method used to solve the problem can be found in
  // algorithms/minknap.c

  long
  profit_knapsack (const SKP_NODE& node);

  // Projects the given SKP_NODE using the best fit algorithm
  //
  // The returning evaluation is a pair containing the projected SKP_NODE and
  // its profit. The best profit uses the given comparison function to decide
  // the best prototype place the next piece.

  SKP_NODE
  best_fit (const SKP_NODE& node,
	    const std::function<bool(const PROTOTYPE&, const PROTOTYPE&)>& comp);

  // Computes the real waste of the given SKP_NODE
  //
  // The real waste of a node is the sum of the area of the MAXIMAL_RECTANGLEs
  // that are not intersected by any other MAXIMAL_RECTANGLE, and cannot contain
  // any piece.

  uint
  real_waste (const SKP_NODE& node);

  // Computes the potential waste of the given SKP_NODE
  //
  // The potential waste of a node is the sum of the areas covered by only one
  // maximal rectangle where the placed PIECE does not fit.

  uint
  potential_waste (const SKP_NODE& node);
}

#endif /* NODE_EVALUATIONS_HPP_ */
