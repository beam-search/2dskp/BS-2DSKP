/*!
 * \file skp_beam_search_node.h
 *
 * \author Carlos Alegría Galicia
 * \date   Apr 23, 2018
 */

#ifndef SKP_NODE_HPP_
#define SKP_NODE_HPP_

#include <vector>
#include <ostream>

#include "../model/maximal_rectangle.hpp"
#include "../model/piece.hpp"
#include "../types.hpp"

namespace two_dim_skp
{

  // A node in the beam search tree. Each node represents the placement of one
  // piece. The sets of pieces and maximal rectangles, as well as the profit,
  // waste, and area are computed after placing the piece corresponding to the
  // node.

  class SKP_NODE
  {
  public:

    //
    // construction and destruction
    //

    // create a node moviendo memoria

    SKP_NODE (
	BIN&& bin,
	PROTOTYPES&& prototypes,
	PROTOTYPES::key_type&& cloned_prototype,
	std::vector<MAXIMAL_RECTANGLE>&& maximal_rectangles);

    // create a node copiando memoria.

    SKP_NODE (
	const BIN& bin,
    	const PROTOTYPES& prototypes,
    	const PROTOTYPES::key_type& cloned_prototype,
    	const std::vector<MAXIMAL_RECTANGLE>& maximal_rectangles);

    // default constructible semantics

    SKP_NODE ();

    virtual
    ~SKP_NODE ();

    // copy semantics

    SKP_NODE (const SKP_NODE&) = default;
    SKP_NODE&
    operator= (const SKP_NODE&) = default;

    // move semantics

    SKP_NODE (SKP_NODE&&) = default;
    SKP_NODE&
    operator= (SKP_NODE&&) = default;

    //
    // methods
    //

    const BIN&
    get_bin () const;

    const PROTOTYPES&
    get_prototypes () const;

    const PROTOTYPE&
    get_cloned_prototype () const;

    bool
    is_cloned_prototype (PROTOTYPES::key_type) const;

    const std::vector<MAXIMAL_RECTANGLE>&
    get_maximal_rectangles () const;

    uint
    get_profit () const;
    uint
    get_waste() const;
    uint
    get_area() const;

    //
    // operators
    //

    std::ostream &
    operator<< (std::ostream &out);

    bool
    operator== (const SKP_NODE& node) const;

  private:

    // the set of pieces already placed up to this point, including the piece
    // corresponding to this node

    BIN bin;

    // the set of prototypes up to this point

    PROTOTYPES prototypes;

    // the prototype used to place the piece corresponding to this node

    PROTOTYPES::key_type cloned_prototype;

    // the maximal rectangles up to this point, after placing the piece
    // corresponding to this node

    std::vector<MAXIMAL_RECTANGLE> maximal_rectangles;

    // the sum of the profits of the pieces placed up to this point, including
    // the piece placed in this node

    uint profit;

    // the sum of the areas of the pieces currently placed

    uint area;

    // the sum of the areas of the maximal rectangles that are not big enough to
    // contain at least one piece from the set of prototypes

    uint waste;


    //
    // auxiliar functions
    //

    // returns the waste of the bin

    uint
    compute_waste (const PROTOTYPES& prototypes,
		   const std::vector<MAXIMAL_RECTANGLE>& maximal_rectangles);
  };

} /* namespace two_dim_skp */

#endif /* SKP_NODE_HPP_ */
