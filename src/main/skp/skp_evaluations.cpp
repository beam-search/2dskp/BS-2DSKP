/*!
 * \file node_evaluations.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jan 28, 2019
 */

#include "skp_evaluations.hpp"

#include <vector>
#include <numeric>
#include <utility>
#include <algorithm>

#include "../types.hpp"
#include "../algorithms/min_knap.hpp"
#include "../algorithms/best_fit.hpp"
#include "../model/rectangle.hpp"
#include "../model/maximal_rectangle.hpp"

namespace two_dim_skp
{
  // TODO The current complexity is O(n m^2 log(m)), where n is the number of
  //      maximal rectangles and m is the number of prototypes

  std::vector<const PROTOTYPE *>
  efficiency_vector (const SKP_NODE& node)
  {
    std::vector<const PROTOTYPE *> max_prototypes;
    boost::rational<uint> max_efficiency (0, 1);
    uint max_area = 0;

    for (auto& rectangle : node.get_maximal_rectangles ())
      {
	std::vector<const PROTOTYPE *> prototypes;

	// obtain the prototypes that fit in the maximal rectangle

	for (auto& prototype : node.get_prototypes ())
	  {
	    if (prototype.second.has_pieces ()
		&& rectangle.get_length () >= prototype.second.get_length ()
		&& rectangle.get_height () >= prototype.second.get_height ())
	      {
		prototypes.push_back (&(prototype.second));
	      }
	  }

	// keep only the subset of prototypes with largest efficiency such that
	// the sum of its area is less than or equal to the area of the
	// rectangle

	std::sort (prototypes.begin (), prototypes.end (), [] (auto& a, auto& b)
	  {
	    return a->get_efficiency () > b->get_efficiency ();
	  });

	uint acc_area = 0;
	auto ptr = std::find_if (
	    prototypes.begin (), prototypes.end (),
	    [&acc_area, &rectangle] (auto& p)
	      {
		if ((acc_area + p->get_area ()) > rectangle.get_area ())
		  {
		    return true;
		  }
		else
		  {
		    acc_area += p->get_area ();
		    return false;
		  }
	      });
	prototypes.erase (ptr, prototypes.end ());

	// update the evaluation set if needed

	auto efficiency = std::accumulate (
	    prototypes.begin (), prototypes.end (),
	    boost::rational<uint> (0, 1),
	    [] (boost::rational<uint>& a, const PROTOTYPE* b)
	      {
		return a + b->get_efficiency ();
	      });

	// brake ties using the maximal rectangle's area

	if ((efficiency > max_efficiency)
	    || (efficiency == max_efficiency && rectangle.get_area () > max_area))
	  {
	    max_efficiency = efficiency;
	    max_prototypes.clear ();
	    std::move (prototypes.begin (), prototypes.end (),
		       std::back_inserter (max_prototypes));
	    max_area = rectangle.get_area ();
	  }
      }

    return std::move (max_prototypes);
  }

  bool
  compare_efficiency_vectors (const std::vector<const PROTOTYPE *>& a,
			      const std::vector<const PROTOTYPE *>& b)
  {
    std::vector<const PROTOTYPE *> eval;

    std::set_difference (a.begin (), a.end (), b.begin (), b.end (),
			 std::back_inserter (eval));

    boost::rational<uint> a_efficiency =
	std::accumulate (eval.begin (), eval.end (),
			 boost::rational<uint> (0,1),
			 [] (boost::rational<uint>& ef, auto& p)
			 {
			    return ef + p->get_efficiency ();
			 });

    eval.clear ();
    std::set_difference (b.begin (), b.end (), a.begin (), a.end (),
			 std::back_inserter (eval));

    boost::rational<uint> b_efficiency =
	std::accumulate (eval.begin (), eval.end (),
			 boost::rational<uint> (0,1),
			 [] (boost::rational<uint>& ef, auto& p)
			 {
			    return ef + p->get_efficiency ();
			 });

    return a_efficiency < b_efficiency;
  }

  long
  profit_knapsack (const SKP_NODE& node)
  {
    std::vector<int> p;
    std::vector<int> w;
    std::vector<int> x;

    for (auto& element : node.get_prototypes ())
      {
	auto& prototype = element.second;
	int count = prototype.get_max_pieces () - prototype.get_cur_pieces ();

	std::fill_n (std::back_inserter (p), count,
		     (int) prototype.get_profit ());
	std::fill_n (std::back_inserter (w), count,
		     (int) prototype.get_area ());
      }

    std::fill_n (std::back_inserter (x), p.size (), 0);
    stype eval = minknap (p.size (), p.data (), w.data (), x.data (),
			  node.get_area () - node.get_waste ());

    return eval;
  }

  SKP_NODE
  best_fit (const SKP_NODE& node,
  	    const std::function<bool(const PROTOTYPE&, const PROTOTYPE&)>& comp)
  {
    // if no more pieces can be placed, return a copy of the given node

    if (!can_place_pieces (node.get_maximal_rectangles (),
			   node.get_prototypes ()))
      {
	return node;
      }

    // otherwise, copy node properties

    BIN bin (node.get_bin ());
    PROTOTYPES prototypes (node.get_prototypes ());
    uint last_prototype;
    std::vector<MAXIMAL_RECTANGLE> maximal_rectangles (
	node.get_maximal_rectangles ());

    // and project node

    best_fit (maximal_rectangles, prototypes, last_prototype,
	      comp, std::back_inserter (bin));

    SKP_NODE projection (std::move (bin), std::move (prototypes),
			 last_prototype, std::move (maximal_rectangles));

    return std::move (projection);
  }

  uint
  real_waste (const SKP_NODE& node)
  {
    auto& rectangles = node.get_maximal_rectangles ();
    auto& prototypes = node.get_prototypes ();
    uint waste = 0;

    for (auto& r : rectangles)
      {
	// check if r is intersected

	if (std::any_of (
	    rectangles.begin (), rectangles.end (),
	    [&r] (auto& r_1)
	    {
	      return r != r_1 && r.is_properly_intersected(r_1);
	    }))
	  {
	    continue;
	  }

	// check if a piece fits in r

	if (std::any_of (
	    prototypes.begin (), prototypes.end (),
	    [&r] (auto& p)
	    {
	      auto& prototype = p.second;
	      return prototype.has_pieces () &&
		  r.get_length () >= prototype.get_length () &&
		  r.get_height () >= prototype.get_height ();
	    }))
	  {
	    waste += r.get_area ();
	  }
      }

    return waste;
  }

  uint
  potential_waste (const SKP_NODE& node)
  {
    auto& prototype = node.get_cloned_prototype ();
    auto& rectangles = node.get_maximal_rectangles ();

    uint waste = 0;
    for (auto& rectangle : rectangles)
      {
	if (rectangle >= prototype)
	  {
	    continue;
	  }

	MAXIMAL_RECTANGLE result (rectangle);
	for (auto& intersecting : rectangles)
	  {
	    result.subtract(intersecting);
	    if (result.get_area() == 0)
	      {
		break;
	      }
	  }

	waste += result.get_area ();
      }

    return waste;
  }

}

