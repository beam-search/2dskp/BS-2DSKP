/*!
 * \file skp_beam_search_node.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Apr 23, 2018
 */

#include "skp_node.hpp"

#include <algorithm>
#include <numeric>


namespace two_dim_skp
{
  //
  // constructors
  //

  SKP_NODE::SKP_NODE (
      BIN&& bin,
      PROTOTYPES&& prototypes,
      PROTOTYPES::key_type&& cloned_prototype,
      std::vector<MAXIMAL_RECTANGLE>&& maximal_rectangles) :
	  bin (bin),
	  prototypes (prototypes),
	  cloned_prototype (cloned_prototype),
	  maximal_rectangles (maximal_rectangles)
  {
    profit = std::accumulate (
	bin.begin (), bin.end (), 0,
	[] (uint a, const PIECE& b) { return a + b.get_profit(); });

    area = std::accumulate (
	bin.begin (), bin.end (), 0,
	[] (uint a, const PIECE& b) { return a + b.get_area(); });

    waste = compute_waste(prototypes, maximal_rectangles);
  }

  SKP_NODE::SKP_NODE (
      const BIN& bin,
      const PROTOTYPES& prototypes,
      const PROTOTYPES::key_type& cloned_prototype,
      const std::vector<MAXIMAL_RECTANGLE>& maximal_rectangles) :
	  bin (bin),
	  prototypes (prototypes),
	  cloned_prototype (cloned_prototype),
	  maximal_rectangles (maximal_rectangles)
  {
    profit = std::accumulate (
	bin.begin (), bin.end (), 0,
    	[] (uint a, const PIECE& b) { return a + b.get_profit(); });

    area = std::accumulate (
    	bin.begin (), bin.end (), 0,
    	[] (uint a, const PIECE& b) { return a + b.get_area(); });

    waste = compute_waste(prototypes, maximal_rectangles);
  }

  SKP_NODE::SKP_NODE () :
      SKP_NODE({{}}, {}, PROTOTYPES::key_type (), {})
  {
  }

  SKP_NODE::~SKP_NODE ()
  {
  }

  //
  // getters
  //

  const BIN&
  SKP_NODE::get_bin () const
  {
    return this->bin;
  }

  const PROTOTYPES&
  SKP_NODE::get_prototypes () const
  {
    return this->prototypes;
  }

  const PROTOTYPE&
  SKP_NODE::get_cloned_prototype () const
  {
    return this->prototypes.at (this->cloned_prototype);
  }

  bool
  SKP_NODE::is_cloned_prototype (PROTOTYPES::key_type index) const
  {
    return index == this->cloned_prototype;
  }

  const std::vector<MAXIMAL_RECTANGLE>&
  SKP_NODE::get_maximal_rectangles () const
  {
    return this->maximal_rectangles;
  }

  uint
  SKP_NODE::get_profit () const
  {
    return this->profit;
  }
  uint
  SKP_NODE::get_waste () const
  {
    return this->waste;
  }
  uint
  SKP_NODE::get_area () const
  {
    return this->area;
  }


  //
  // auxiliary
  //

  uint
  SKP_NODE::compute_waste (
      const PROTOTYPES& prototypes,
      const std::vector<MAXIMAL_RECTANGLE>& maximal_rectangles)
  {
    // TODO currently computing full waste (double counting intersections) in
    //      O(nm) time, where n is the number of maximal rectangles and m is the
    //      number of prototypes.

    uint waste = 0;
    for (auto& maximal_rectangle : maximal_rectangles)
      {
	for (auto& prototype : prototypes)
	  {
	    if (prototype.second.has_pieces () &&
		maximal_rectangle.get_length () >=
		prototype.second.get_length () &&
		maximal_rectangle.get_height () >=
		prototype.second.get_height ())
	      {
		goto FIT;
	      }
	  }
	waste += maximal_rectangle.get_area ();
	FIT: continue;
      }

    return waste;
  }

  //
  // operators
  //

  std::ostream&
  SKP_NODE::operator<< (std::ostream& out)
  {
    out << "profit = " << this->profit
	<< ", area = " << this->area
	<< ", waste = " << this->waste
	<< ", prototype = " << this->cloned_prototype
	<< ", prototypes:" << std::endl;

    for_each(this->prototypes.begin(), this->prototypes.end(),
	     [&out] (const PROTOTYPES::value_type& v)
	     {
		out << v.second << std::endl;
	     });

    out << "pieces:" << std::endl;
    for_each(this->bin.begin(), this->bin.end(),
    	     [&out] (const PIECE& p)
    	     {
		out << p << std::endl;
    	     });

    out << "maximal rectangles:" << std::endl;
    for_each(this->maximal_rectangles.begin(), this->maximal_rectangles.end(),
	     [&out] (const MAXIMAL_RECTANGLE& r)
	     {
		out << "min x = " << r.get_min_x()
		    << ", min y = " << r.get_min_y()
		    << ", max x = " << r.get_max_x()
		    << ", max y = " << r.get_max_y()
		    << ", area = " << r.get_area()
		    << ", perimeter = " << r.get_perimeter()
		    << std::endl;
	     });

    return out;
  }

  bool
  SKP_NODE::operator== (const SKP_NODE& node) const
  {
    // TODO Change to a more efficient algorithm. Current set equality consumes
    // O(n²) time

    const auto& bin = node.get_bin ();

    std::vector<size_t> indexes (bin.size ());
    std::iota (indexes.begin (), indexes.end (), 0);

    for (const auto& piece : this->get_bin ())
      {
	auto it = std::find_if (
	    indexes.begin (), indexes.end (),
    	    [&piece,bin] (size_t index)
	    {
    	      return (piece.get_min_x () == bin[index].get_min_x ())
    		  && (piece.get_min_y () == bin[index].get_min_y ())
		  && (piece.get_max_x () == bin[index].get_max_x ())
		  && (piece.get_max_y () == bin[index].get_max_y ());
    	    });
    	if (it == indexes.end ())
    	  {
    	    return false;
    	  }
    	indexes.erase(it);
      }

    return true;
  }

} /* namespace two_dim_skp */
