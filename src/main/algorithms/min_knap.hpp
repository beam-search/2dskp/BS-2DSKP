/*!
 * \file min_knap.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jan 23, 2019
 */

#ifndef MIN_KNAP_HPP_
#define MIN_KNAP_HPP_

extern "C"
{
  typedef long stype;
  stype minknap(int n, int *p, int *w, int *x, int c);
}

#endif /* MIN_KNAP_HPP_ */
