/*!
 * \file place1piece.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jun 17, 2018
 */

#include "place_piece.hpp"

namespace two_dim_skp
{

  std::vector<MAXIMAL_RECTANGLE>
  create_maximal_rectangles (uint bin_length, uint bin_height)
  {
    return
	{ { (uint) 0, (uint) 0, bin_length, bin_height, // the rectangle
  	  { 0, bin_length },				// top intervals
  	  { 0, bin_length },				// bottom intervals
  	  { 0, bin_height },				// left intervals
  	  { 0, bin_height } }	 			// right intervals
	};
  }

} /* namespace two_dim_skp */
