/*!
 * \file difference_process_placement.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   May 15, 2018
 */

#ifndef PLACEMENT_POSITIONS_HPP_
#define PLACEMENT_POSITIONS_HPP_

#include <numeric>

#include "../model/piece.hpp"
#include "../model/prototype.hpp"
#include "../model/maximal_rectangle.hpp"

namespace two_dim_skp
{
  // The prototype is placed in the bottom left corner of rectangle.

  PIECE
  bottom_left_corner (PROTOTYPE& prototype,
		      const PLACEABLE_RECTANGLE& rectangle);

  // first & last: iterators to the list of all MR
  // rectangle: corners (min_x, min_y; max_x max_y) of the piece depending on
  // the corner of the MR where it is placed.
  // Returns:

  template<class InputIt>
  uint
  placement_area (InputIt first, InputIt last,
		  const PLACEABLE_RECTANGLE& rectangle)
  {
    uint result = 0;
    for (; first != last; ++first)
      {
	auto& r = *first;
	if (!rectangle.is_properly_intersected (r))
	  {
	    continue;
	  }

	uint top = rectangle.get_max_y () < r.get_max_y () ?
	    rectangle.get_max_y () : r.get_max_y ();

	uint bottom = rectangle.get_min_y () > r.get_min_y () ?
	    rectangle.get_min_y () : r.get_min_y ();

	uint left = rectangle.get_min_x () < r.get_min_x () ?
	    rectangle.get_min_x () : r.get_min_x ();

	uint right = rectangle.get_max_x () > r.get_max_x () ?
	    rectangle.get_max_x () : r.get_max_x ();

	result += r.get_area () - (top - bottom) * (right - left);
      }

    return result;
  }

  // This function places the given prototype in the best corner of the selected
  // maximal rectangle.
  // Best corner: Corner that produces the biggest maximal rectangles.

  template<template<typename, typename ...> class Container>
  PIECE
  best_corner (const Container<MAXIMAL_RECTANGLE>& all_maxrect,
	       const MAXIMAL_RECTANGLE& maxrect, PROTOTYPE& prototype)
  {
    // If MR has the same dimensions as the prototype, place piece in bottom-left
    if (maxrect.get_length() == prototype.get_length()
	&& maxrect.get_height() == prototype.get_height())
      {
	return std::move(bottom_left_corner(prototype, maxrect));
      }

    uint area;
    uint x;
    uint y;

    uint best_area;
    uint best_x = maxrect.get_min_x();
    uint best_y = maxrect.get_min_y();

    // bottom left

    best_area = placement_area(all_maxrect.begin(),
			       all_maxrect.end(),
			       PLACEABLE_RECTANGLE(best_x, best_y,
						   prototype.get_length(),
						   prototype.get_height()));

    // top left

    x = maxrect.get_min_x();
    y = maxrect.get_max_y() - prototype.get_height();
    area = placement_area(all_maxrect.begin(),
			  all_maxrect.end(),
			  PLACEABLE_RECTANGLE(x, y,
					      prototype.get_length(),
					      prototype.get_height()));
    if (area > best_area)
      {
	best_area = area;
	best_x = x;
	best_y = y;
      }

    // bottom right

    x = maxrect.get_max_x() - prototype.get_length();
    y = maxrect.get_min_y();
    area = placement_area(all_maxrect.begin(),
			  all_maxrect.end(),
			  PLACEABLE_RECTANGLE(x, y,
					      prototype.get_length(),
    					      prototype.get_height()));
    if (area > best_area)
      {
	best_area = area;
	best_x = x;
	best_y = y;
      }

    // top right

    x = maxrect.get_max_x() - prototype.get_length();
    y = maxrect.get_max_y() - prototype.get_height();
    area = placement_area(all_maxrect.begin(),
			  all_maxrect.end(),
			  PLACEABLE_RECTANGLE(x, y,
					      prototype.get_length(),
					      prototype.get_height()));
    if (area > best_area)
      {
    	best_x = x;
    	best_y = y;
      }

    return std::move (prototype.place (best_x, best_y));
  }

} /* namespace two_dim_skp */

#endif /* PLACEMENT_POSITIONS_HPP_ */
