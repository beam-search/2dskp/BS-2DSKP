/*!
 * \file grasp.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Feb 6, 2018
 */

#ifndef PLACE_PIECE_HPP_
#define PLACE_PIECE_HPP_

#include <iterator>
#include <vector>

#include "../algorithms/place_piece_update_mr.hpp"
#include "../model/maximal_rectangle.hpp"
#include "../model/prototype.hpp"
#include "../model/piece.hpp"
#include "place_piece_piece_placement.hpp"


namespace two_dim_skp
{
  // Helper function to create the maximal rectangle for a bin with no pieces
  // placed (it fills the entire bin)

  std::vector<MAXIMAL_RECTANGLE>
  create_maximal_rectangles (uint bin_length, uint bin_height);

  //

  template<template<typename, typename ...> class Container>
  Container<MAXIMAL_RECTANGLE>
  update_maximal_rectangles (
      const Container<MAXIMAL_RECTANGLE>& maximal_rectangles,
      const PIECE& piece)
  {
    Container<MAXIMAL_RECTANGLE> new_rectangles;

    // detecting maximal rectangles that need to be updated

    auto out = std::back_inserter (new_rectangles);
    for (auto& rectangle : maximal_rectangles)
      {
	auto new_rectangle = rectangle;
	bool result = update_mr (new_rectangle, piece, out);
	if (!result)
	  {
	    out = std::move(new_rectangle);
	  }
      }

    // return updated MAXIMAL_RECTANGLEs

    return std::move(new_rectangles);
  }

  // Piece placement functions
  //
  // A PIECE is placed from the given PROTOTYPE in the MAXIMAL_RECTANGLE with
  // smallest area possible. The PIECE is placed in the corner of the
  // MAXIMAL_RECTANGLE such that the vector formed by the sorted set of areas of
  // the induced maximal rectangles is the biggest in lexicographical order.

  // Test if there is a maximal rectangle in the SequenceContainer where a piece
  // of the given PROTOTYPE can be placed.
  //
  // If this function returns true, then the function place1piece should place
  // a piece.
  //
  template<template<typename, typename ...> class SequenceContainer>
  bool
  can_place_piece (
      const SequenceContainer<MAXIMAL_RECTANGLE>& maximal_rectangles,
      const PROTOTYPE& prototype)
  {
    if (!prototype.has_pieces())
      {
	return false;
      }

    auto end = maximal_rectangles.cend();
    auto max = find_if (
	maximal_rectangles.cbegin(),
	end,
	[&prototype] (const MAXIMAL_RECTANGLE& rect)
	{
	  return (rect.get_length () >= prototype.get_length ()) &&
	      (rect.get_height () >= prototype.get_height ());
	});

    return (max == end) ? false: true;
  }

  // Place PIECEs of the given Prototype in the MAXIMAL_RECTANGLEs of the given
  // SKP_BS_FILTER::NODE_TYPE.
  //
  // The PIECEs are placed in the four corners of all MAXIMAL_RECTANGLEs that
  // fits the given PROTOTYPE. The placement is such that there are no repeated
  // placements, and at least two sides of each PIECE touches another PIECE.

  template<class InputIt, class Compare>
    bool
    test_placement (InputIt first, InputIt last, uint key_start, uint key_end,
		    Compare comp)
    {
      auto start_ptr = std::lower_bound (first, last, key_start, comp);
      auto end_ptr = std::lower_bound (first, last, key_end, comp);
      return start_ptr != end_ptr || ((end_ptr - first) + 1) % 2 == 0;
    }

  // Places a PIECE from the given PROTOTYPE in the MAXIMAL_RECTANGLE from the
  // given SequenceContainer with the smallest area possible.
  //
  // - The placed PIECE is returned.
  //
  // - It assumes that the function can_place_piece is guaranteed to return
  //   true. Otherwise behavior is unknown.

  template<template<typename, typename ...> class SequenceContainer, class PrOutputIt, class ROutputIt, class POutputIt>
  void
  place_pieces (
      const SequenceContainer<MAXIMAL_RECTANGLE>& maximal_rectangles,
      const PROTOTYPES& prototypes,
      PROTOTYPES::key_type index,
      PrOutputIt pr_out, ROutputIt r_out, POutputIt p_out)
  {

    // created PIECEs

    std::vector<PLACEABLE_RECTANGLE> pieces;

    // add PIECE and maximal rectangles

    auto add_piece = [&] (PLACEABLE_RECTANGLE r) -> void
	{
	  PROTOTYPES new_pr = prototypes;
	  PIECE new_p = new_pr[index].place (r.get_min_x (), r.get_min_y ());
	  std::vector<MAXIMAL_RECTANGLE> new_mr = update_maximal_rectangles(maximal_rectangles, new_p);

	  pieces.push_back(r);
	  pr_out = std::move (new_pr);
	  r_out = std::move (new_mr);
	  p_out = std::move (new_p);
	};

    // do while there is a MAXIMAL_RECTANGLE that fits the given PROTOTYPE

    auto it = maximal_rectangles.begin ();
    auto end = maximal_rectangles.end ();
    auto& prototype = prototypes.at(index);

    while ((it = std::find_if (
	  it, end,
	  [&prototype] (const auto& r) { return r >= prototype; })) != end)
      {
	auto& rectangle = *(it++);

	//
	// PROTOTYPE is equal to MAXIMAL_RECTANGLE
	//

	if (((PLACEABLE_RECTANGLE&) prototype) ==
	    ((PLACEABLE_RECTANGLE&) rectangle))
	  {
	    add_piece (rectangle);
	    continue;
	  }

	auto& top = rectangle.get_top ();
	auto& bottom = rectangle.get_bottom ();
	auto& left = rectangle.get_left ();
	auto& right = rectangle.get_right ();

	auto right_min_x = rectangle.get_max_x () - prototype.get_length ();
	auto top_min_y = rectangle.get_max_y () - prototype.get_height ();

	//
	// PROTOTYPE's length is equal to MAXIMAL_RECTANGLE's
	//

	if (prototype.get_length () == rectangle.get_length ())
	  {
	    // bottom

	    {
	      PLACEABLE_RECTANGLE p_r (rectangle.get_min_x (),
				       rectangle.get_min_y (),
				       prototype.get_length (),
				       prototype.get_height ());

	      if (std::find (pieces.begin (), pieces.end (), p_r) ==
		  pieces.end ())
		{
		  uint count = 0;

		    count += test_placement (bottom.begin (),
					     bottom.end (),
					     p_r.get_min_x (),
					     p_r.get_max_x (),
					     std::less<uint> ()) ? 1 : 0;

		    count += test_placement (left.begin (),
					     left.end (),
					     p_r.get_min_y (),
					     p_r.get_max_y (),
					     std::less<uint> ()) ? 1 : 0;

		    count += test_placement (right.begin (),
					     right.end (),
					     p_r.get_min_y (),
					     p_r.get_max_y (),
					     std::less<uint> ()) ? 1 : 0;

		    if (count >= 2) add_piece (p_r);
		  }
	      }

	      // top

	      {
		PLACEABLE_RECTANGLE p_r (rectangle.get_min_x(),
					 top_min_y,
					 prototype.get_length (),
					 prototype.get_height ());

		if (std::find (pieces.begin (), pieces.end (), p_r) ==
		    pieces.end ())
		  {
		    uint count = 0;

		    count += test_placement (top.begin (),
					     top.end (),
					     p_r.get_min_x (),
					     p_r.get_max_x (),
					     std::less<uint> ()) ? 1 : 0;

		    count += test_placement (left.rbegin (),
					     left.rend (),
					     p_r.get_max_y (),
					     p_r.get_min_y (),
					     std::greater<uint> ()) ? 1 : 0;

		    count += test_placement (right.rbegin (),
					     right.rend (),
					     p_r.get_max_y (),
					     p_r.get_min_y (),
					     std::greater<uint> ()) ? 1 : 0;

		    if (count >= 2) add_piece (p_r);
		  }
	      }

	      continue;
	    }

	  //
	  // PROTOTYPE's height is equal to MAXIMAL_RECTANGLE's
	  //

	  if (prototype.get_height () == rectangle.get_height ())
	    {
	      // left

	      {
		PLACEABLE_RECTANGLE p_r (rectangle.get_min_x (),
					 rectangle.get_min_y (),
					 prototype.get_length (),
					 prototype.get_height ());

		if (std::find (pieces.begin (), pieces.end (), p_r) ==
		    pieces.end ())
		  {
		    uint count = 0;

		    count += test_placement (left.begin (),
					     left.end (),
					     p_r.get_min_y (),
					     p_r.get_max_y (),
					     std::less<uint> ()) ? 1 : 0;

		    count += test_placement (top.begin (),
					     top.end (),
					     p_r.get_min_y (),
					     p_r.get_max_y (),
					     std::less<uint> ()) ? 1 : 0;

		    count += test_placement (bottom.begin (),
					     bottom.end (),
					     p_r.get_min_x (),
					     p_r.get_max_x (),
					     std::less<uint> ()) ? 1 : 0;

		    if (count >= 2) add_piece (p_r);
		  }
	      }

	      // right

	      {
		PLACEABLE_RECTANGLE p_r (right_min_x,
					 rectangle.get_min_y (),
					 prototype.get_length (),
					 prototype.get_height ());

		if (std::find (pieces.begin (), pieces.end (), p_r) ==
		    pieces.end ())
		  {
		    uint count = 0;

		    count += test_placement (right.begin (),
					     right.end (),
					     p_r.get_min_y (),
					     p_r.get_max_y (),
					     std::less<uint> ()) ? 1 : 0;

		    count += test_placement (top.rbegin (),
					     top.rend (),
					     p_r.get_max_x (),
					     p_r.get_min_x (),
					     std::greater<uint> ()) ? 1 : 0;

		    count += test_placement (bottom.rbegin (),
					     bottom.rend (),
					     p_r.get_max_x (),
					     p_r.get_min_x (),
					     std::greater<uint> ()) ? 1 : 0;

		    if (count >= 2) add_piece (p_r);
		  }
	      }

	      continue;
	    }

	  //
	  // PROTOTYPE is strictly smaller than the MAXIMAL_RECTANGLE
	  //

	  // bottom-left

	  {
	    PLACEABLE_RECTANGLE p_r (rectangle.get_min_x (),
				     rectangle.get_min_y (),
				     prototype.get_length (),
				     prototype.get_height ());

	    if (std::find (pieces.begin (), pieces.end (), p_r) ==
		pieces.end () &&
		test_placement (bottom.begin (), bottom.end (),
				p_r.get_min_x (),
				p_r.get_max_x (),
				std::less<uint> ()) &&
		test_placement (left.begin (), left.end (),
				p_r.get_min_y (),
				p_r.get_max_y (),
				std::less<uint> ()))
	      {
		add_piece (p_r);
	      }
	  }


	  // bottom-right

	  {
	    PLACEABLE_RECTANGLE p_r (right_min_x,
				     rectangle.get_min_y (),
				     prototype.get_length (),
				     prototype.get_height ());

	    if (std::find (pieces.begin (), pieces.end (), p_r) ==
		pieces.end () &&
		test_placement (bottom.rbegin (), bottom.rend (),
				p_r.get_max_x (),
				p_r.get_min_x (),
				std::greater<uint> ()) &&
	        test_placement (right.begin (), right.end (),
				p_r.get_min_y (),
				p_r.get_max_y (),
				std::less<uint> ()))
	      {
		add_piece (p_r);
	      }
	  }

	  // top-left corner

	  {
	    PLACEABLE_RECTANGLE p_r (rectangle.get_min_x (),
				     top_min_y,
				     prototype.get_length (),
				     prototype.get_height ());

	    if (std::find (pieces.begin (), pieces.end (), p_r) ==
		pieces.end () &&
		test_placement (top.begin (), top.end (),
				p_r.get_min_x (),
				p_r.get_max_x (),
				std::less<uint> ()) &&
	        test_placement (left.rbegin (), left.rend (),
				p_r.get_max_y (),
				p_r.get_min_y (),
				std::greater<uint> ()))
	      {
		add_piece (p_r);
	      }
	  }

	  // top-right

	  {
	    PLACEABLE_RECTANGLE p_r (right_min_x,
				     top_min_y,
				     prototype.get_length (),
				     prototype.get_height ());

	    if (std::find (pieces.begin (), pieces.end (), p_r) ==
		pieces.end () &&
		test_placement (top.rbegin (), top.rend (),
				p_r.get_max_x (),
				p_r.get_min_x (),
				std::greater<uint> ()) &&
	  	test_placement (right.rbegin (), right.rend (),
				p_r.get_max_y (),
				p_r.get_min_y (),
				std::greater<uint> ()))
	      {
		add_piece (p_r);
	      }
	  }
	}
    }
} /* namespace two_dim_skp */

#endif /* PLACE_PIECE_HPP_ */
