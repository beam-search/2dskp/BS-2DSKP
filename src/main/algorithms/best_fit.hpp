/*!
 * \file best_fit_mr.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jun 18, 2018
 */

#ifndef BEST_FIT_HPP_
#define BEST_FIT_HPP_

#include <limits>
#include <vector>

#include "../algorithms/placement_positions.hpp"
#include "../algorithms/best_fit_types.hpp"
#include "../model/maximal_rectangle.hpp"
#include "../model/prototype.hpp"
#include "../types.hpp"
#include "place_piece_update_mr.hpp"

namespace two_dim_skp
{
  /// Test if there is a MAXIMAL_RECTANGLE in the SequenceContainer where a
  /// PIECE of at least one PROTOTYPE can be placed.
  ///
  template<template<typename, typename ...> class SequenceContainer>
    bool
    can_place_pieces (
	const SequenceContainer<MAXIMAL_RECTANGLE>& maximal_rectangles,
	const PROTOTYPES& prototypes)
  {
    for (auto& el : prototypes)
      {
	auto& prototype = el.second;
	if (!prototype.has_pieces())
	  {
	    continue;
	  }

	for (const MAXIMAL_RECTANGLE& rectangle : maximal_rectangles)
	  {
	    if (rectangle.get_length () >= prototype.get_length () &&
		rectangle.get_height () >= prototype.get_height())
	      {
		return true;
	      }
	  }
      }

    return false;
  }

  /// best fit with maximal rectangles
  ///
  /// Dynamically select prototypes and place all possible pieces. It assumes
  /// the function can_place_pieces returns true. Piece location is selected in
  /// a greedy way:
  ///
  /// - We sort the maximal rectangles by the position of its bottom left
  ///   corner. From bottom to top and left to right.
  ///
  /// - We traverse the sorted set of maximal rectangles and choose the
  ///   PROTOTYPE with highest value to place a PIECE. Given a pair of
  ///   PROTOTYPEs, the one with highest value if tested using the given
  ///   comparison functor.
  ///
  /// - The PIECE is placed in the bottom left corner of the MAXIMAL_RECTANGLE
  ///
  /// - The process is repeated until no PROTOTYPE fits in a MAXIMAL_RECTANGLE,
  ///   or there are no PROTOTYPEs with available PIECEs.
  ///
  ///
  /// \param maximal_rectangles The container where the set of maximal
  ///				rectangles are stored.
  /// \param prototypes		The set PROTOTYPEs representing the problem
  ///				instance.
  /// \param last_prototype	The index of the last prototype cloned during
  ///				the placement process.
  /// \param out		The output iterator where the placed PIECEs are
  ///				going to be stored.
  ///
  template<template<typename, typename ...> class Container, class Compare, class OutputIterator>
  void
  best_fit (Container<MAXIMAL_RECTANGLE>& maximal_rectangles,
	    PROTOTYPES& prototypes, uint& last_prototype, Compare comp,
	    OutputIterator out)
  {
    bool keep_placing = true;
    while (keep_placing)
      {
	keep_placing = false;
	MR_V mr_vert(maximal_rectangles.begin (), maximal_rectangles.end ());

	for (auto& maximal_rectangle : mr_vert)
	  {
	    uint prototype_index = std::numeric_limits<uint>::max ();
	    for (auto it = prototypes.begin (), end = prototypes.end ();
		it != end;)
	      {
		// remove prototypes without pieces

		if (!(*it).second.has_pieces())
		  {
		    it = prototypes.erase(it);
		    end = prototypes.end();
		    continue;
		  }

		// keep the best prototype fitting in the MAXIMAL_RECTANGLE,
		// according to the given comparison function

		auto& prototype = it->second;
		if (maximal_rectangle.get_length() >= prototype.get_length()
		    && maximal_rectangle.get_height() >= prototype.get_height())
		  {
		    if (comp(prototypes[prototype_index], prototype)
			|| prototype_index == std::numeric_limits<uint>::max())
		      {
			prototype_index = it->first;
		      }
		  }
		++it;
	      }

	    // no fitting prototype found

	    if (prototype_index == std::numeric_limits<uint>::max())
	      {
		continue;
	      }

	    // place piece on the bottom-left corner of the rectangle

	    last_prototype = prototype_index;
	    PIECE piece = bottom_left_corner(prototypes[prototype_index],
					     maximal_rectangle);

	    // update affected maximal rectangles

	    std::vector<MAXIMAL_RECTANGLE> new_rectangles;
	    auto out_new = std::back_inserter (new_rectangles);
	    for (auto it = maximal_rectangles.begin (),
		end = maximal_rectangles.end ();
		it != end;)
	    {
	      auto& maximal_rectangle = *it;
	      bool result = update_mr (maximal_rectangle, piece, out_new);
	      if (result)
		{
		  it = maximal_rectangles.erase (it);
		  end = maximal_rectangles.end ();
		}
	      else
		{
		  ++it;
		}
	    }

	    // update sets of pieces and maximal rectangles

	    std::move (new_rectangles.begin (), new_rectangles.end (),
	    	       std::back_inserter(maximal_rectangles));
	    out = std::move(piece);

	    // keep placing pieces

	    keep_placing = true;
	    break;
	  }
      }
  }

} /* namespace two_dim_skp */

#endif /* BEST_FIT_HPP_ */
