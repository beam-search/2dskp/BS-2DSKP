/*!
 * \file bem_search_mr_func.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jun 18, 2018
 */

#ifndef BEST_FIT_MR_FUNC_HPP_
#define BEST_FIT_MR_FUNC_HPP_

#include <queue>
#include <set>

#include "../model/maximal_rectangle.hpp"

namespace two_dim_skp
{
  // The set of maximal rectangles ordered by non-decreasing y-coordinate
  // (from bottom to top) and non-decreasing x-coordinate (from left to right).

  struct NON_DECREASING_YX_MR
  {
    bool
    operator() (const MAXIMAL_RECTANGLE& a, const MAXIMAL_RECTANGLE& b) const
    {
      return
	  a.get_min_y () != b.get_min_y () ?
	      a.get_min_y () < b.get_min_y () : a.get_min_x () < b.get_min_x ();
    }
  };
  typedef std::set<MAXIMAL_RECTANGLE, NON_DECREASING_YX_MR> MR_V;

  // Bottom most gaps
  //
  // A priority queue containing the gaps with the same y-coordinate, to
  // retrieve the widest gap. Comparator orders the gaps by non-decreasing width

  struct DOMINANCE_LEGTH_HEIGHT_AREA
  {
    bool
    operator() (const MAXIMAL_RECTANGLE& a, const MAXIMAL_RECTANGLE& b) const
    {
      return a.get_length() <= b.get_length()
	  && a.get_height() <= b.get_height()
	  && a.get_area() <= b.get_area();
    }
  };
  typedef std::priority_queue<MAXIMAL_RECTANGLE, std::vector<MAXIMAL_RECTANGLE>,
      DOMINANCE_LEGTH_HEIGHT_AREA> MR_L;

} /* namespace two_dim_skp */

#endif /* BEST_FIT_MR_FUNC_HPP_ */
