/*!
 * \file placement_positions.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Jun 17, 2018
 */

#include "../algorithms/placement_positions.hpp"

namespace two_dim_skp
{
  PIECE
  bottom_left_corner (PROTOTYPE& prototype,
		      const PLACEABLE_RECTANGLE& rectangle)
  {
    return std::move (
	prototype.place (rectangle.get_min_x (), rectangle.get_min_y ()));
  }
} /* namespace two_dim_skp */

