//
//  update_MR.h
//  BS-2DSKP
//
//  Created by Marta Cabo Nodar on 13/06/18.
//  Copyright © 2018 Marta Cabo. All rights reserved.
//

#ifndef update_MR_h
#define update_MR_h

#include <vector>
#include <iterator>
#include <algorithm>
using namespace std;

#include "../model/piece.hpp"
#include "../model/prototype.hpp"
#include "../model/maximal_rectangle.hpp"

#include "../algorithms/placement_positions.hpp"

namespace two_dim_skp
{
    template<class InputIt, class Compare>
    bool
    is_maximal(InputIt first, InputIt last, uint key, Compare comp)
    {
        auto ptr = lower_bound(first, last, key, comp);
        return first != ptr;
    }
    
    
    template<class InputIt, class OutputIt, class Compare>
    void
    cut_intervals(InputIt first, InputIt last, OutputIt out,
                  uint key, Compare comp)
    {
        InputIt end = upper_bound(first, last, key, comp);
        copy(first, end, out);
        if (((end - first) + 1) % 2 == 0)
        {
            out = key;
        }
    }
    
    
    template<class OutputIterator>
    bool
    update_mr (MAXIMAL_RECTANGLE& rectangle, const PIECE& piece,
	       OutputIterator out)
    {
      // =======================================================================
      // if MR does not intersect with the given piece, the MR does not change

      if (!rectangle.is_intersected (piece))
	{
	  return false;
	}

      // if the MR is equal to the given piece, return true
      // MR will be eliminated.

      if (rectangle == piece)
	{
	  return true;
	}

      // =======================================================================
      // Check if piece is "orbiting" the MR: outside the MR, but with one
      // concurrent edge. In these cases the MR does not change, but intervals
      // need update.

      if (rectangle.get_max_y () == piece.get_min_y ())
	{
	  rectangle.update_top (piece.get_min_x (), piece.get_max_x ()); //Piece is on top of the MR
	  return false;
	}
      if (rectangle.get_min_y () == piece.get_max_y ())
	{
	  rectangle.update_bottom (piece.get_min_x (), piece.get_max_x ()); //Piece is below MR
	  return false;
	}
      if (rectangle.get_max_x () == piece.get_min_x ())
	{
	  rectangle.update_right (piece.get_min_y (), piece.get_max_y ()); //Piece is to the right of the MR
	  return false;
	}
      if (rectangle.get_min_x () == piece.get_max_x ())
	{
	  rectangle.update_left (piece.get_min_y (), piece.get_max_y ()); //Piece is to the left of the MR
	  return false;
	}

      // =======================================================================
      // if there is a maximal rectangle to the left of the piece, create the
      // rectangle and add it to the output iterator

      if (rectangle.get_min_x () < piece.get_min_x ()
	  && is_maximal (rectangle.get_top ().begin (),
			 rectangle.get_top ().end (), piece.get_min_x (),
			 less<uint> ())
	  && is_maximal (rectangle.get_bottom ().begin (),
			 rectangle.get_bottom ().end (), piece.get_min_x (),
			 less<uint> ()))
	{
	  MAXIMAL_RECTANGLE left_rectangle (
	      rectangle.get_min_x (), rectangle.get_min_y (),
	      piece.get_min_x () - rectangle.get_min_x (),
	      rectangle.get_height ());

	  cut_intervals (rectangle.get_top ().begin (),
			 rectangle.get_top ().end (),
			 back_inserter (left_rectangle.get_top ()),
			 piece.get_min_x (), less<uint> ());

	  cut_intervals (rectangle.get_top ().begin (),
			 rectangle.get_top ().end (),
			 back_inserter (left_rectangle.get_bottom ()),
			 piece.get_min_x (), less<uint> ());

	  copy (rectangle.get_left ().begin (), rectangle.get_left ().end (),
		back_inserter (left_rectangle.get_left ()));

	  left_rectangle.get_right ().push_back (
	      std::max (rectangle.get_min_y (), piece.get_min_y ()));
	  left_rectangle.get_right ().push_back (
	      std::min (rectangle.get_max_y (), piece.get_max_y ()));

	  out = std::move (left_rectangle);
	}

      // test if there is a maximal rectangle to the right of the piece

      if (rectangle.get_max_x () > piece.get_max_x ()
	  && is_maximal (rectangle.get_top ().rbegin (),
			 rectangle.get_top ().rend (), piece.get_max_x (),
			 greater<uint> ())
	  && is_maximal (rectangle.get_bottom ().rbegin (),
			 rectangle.get_bottom ().rend (), piece.get_max_x (),
			 greater<uint> ()))
	{
	  MAXIMAL_RECTANGLE right_rectangle (
	      piece.get_max_x (), rectangle.get_min_y (),
	      rectangle.get_max_x () - piece.get_max_x (),
	      rectangle.get_height ());

	  cut_intervals (rectangle.get_top ().rbegin (),
			 rectangle.get_top ().rend (),
			 back_inserter (right_rectangle.get_top ()),
			 piece.get_max_x (), greater<uint> ());
	  reverse (right_rectangle.get_top ().begin (),
		   right_rectangle.get_top ().end ());

	  cut_intervals (rectangle.get_bottom ().rbegin (),
			 rectangle.get_bottom ().rend (),
			 back_inserter (right_rectangle.get_bottom ()),
			 piece.get_max_x (), greater<uint> ());
	  reverse (right_rectangle.get_bottom ().begin (),
		   right_rectangle.get_bottom ().end ());

	  copy (rectangle.get_right ().begin (), rectangle.get_right ().end (),
		back_inserter (right_rectangle.get_right ()));

	  right_rectangle.get_left ().push_back (
	      max (rectangle.get_min_y (), piece.get_min_y ()));
	  right_rectangle.get_left ().push_back (
	      min (rectangle.get_max_y (), piece.get_max_y ()));

	  out = std::move (right_rectangle.update ());
	}

      // test if there is a maximal rectangle to the top of the piece

      if (rectangle.get_max_y () > piece.get_max_y ()
	  && is_maximal (rectangle.get_left ().rbegin (),
			 rectangle.get_left ().rend (), piece.get_max_y (),
			 greater<uint> ())
	  && is_maximal (rectangle.get_right ().rbegin (),
			 rectangle.get_right ().rend (), piece.get_max_y (),
			 greater<uint> ()))
	{
	  MAXIMAL_RECTANGLE top_rectangle (
	      rectangle.get_min_x (), piece.get_max_y (),
	      rectangle.get_length (),
	      rectangle.get_max_y () - piece.get_max_y ());

	  cut_intervals (rectangle.get_left ().rbegin (),
			 rectangle.get_left ().rend (),
			 back_inserter (top_rectangle.get_left ()),
			 piece.get_max_y (), greater<uint> ());
	  reverse (top_rectangle.get_left ().begin (),
		   top_rectangle.get_left ().end ());

	  cut_intervals (rectangle.get_right ().rbegin (),
			 rectangle.get_right ().rend (),
			 back_inserter (top_rectangle.get_right ()),
			 piece.get_max_y (), greater<uint> ());
	  reverse (top_rectangle.get_right ().begin (),
		   top_rectangle.get_right ().end ());

	  copy (rectangle.get_top ().begin (), rectangle.get_top ().end (),
		back_inserter (top_rectangle.get_top ()));

	  top_rectangle.get_bottom ().push_back (
	      max (rectangle.get_min_x (), piece.get_min_x ()));
	  top_rectangle.get_bottom ().push_back (
	      min (rectangle.get_max_x (), piece.get_max_x ()));

	  out = std::move (top_rectangle.update ());
	}

      // test if there is a maximal rectangle to the bottom of the piece

      if (rectangle.get_min_y () < piece.get_min_y ()
	  && is_maximal (rectangle.get_left ().begin (),
			 rectangle.get_left ().end (), piece.get_min_y (),
			 less<uint> ())
	  && is_maximal (rectangle.get_right ().begin (),
			 rectangle.get_right ().end (), piece.get_min_y (),
			 less<uint> ()))
	{
	  MAXIMAL_RECTANGLE bottom_rectangle (
	      rectangle.get_min_x (), rectangle.get_min_y (),
	      rectangle.get_length (),
	      piece.get_min_y () - rectangle.get_min_y ());

	  cut_intervals (rectangle.get_left ().begin (),
			 rectangle.get_left ().end (),
			 back_inserter (bottom_rectangle.get_left ()),
			 piece.get_min_y (), less<uint> ());
	  cut_intervals (rectangle.get_right ().begin (),
			 rectangle.get_right ().end (),
			 back_inserter (bottom_rectangle.get_right ()),
			 piece.get_min_y (), less<uint> ());

	  copy (rectangle.get_bottom ().begin (),
		rectangle.get_bottom ().end (),
		back_inserter (bottom_rectangle.get_bottom ()));

	  bottom_rectangle.get_top ().push_back (
	      max (rectangle.get_min_x (), piece.get_min_x ()));
	  bottom_rectangle.get_top ().push_back (
	      min (rectangle.get_max_x (), piece.get_max_x ()));

	  out = std::move (bottom_rectangle.update ());
	}
      return true;
    }
    
    ///
    /// This function creates the coordinates of the possible MR defined if the
    /// piece would be place in the given position
    ///
    template<class OutputIterator>
    bool
    possible_mr (MAXIMAL_RECTANGLE& rectangle, const PLACEABLE_RECTANGLE& piece,
		 OutputIterator out)
    {
      // =======================================================================
      // if MR does not intersect with the given piece, the MR does not change

      if (!rectangle.is_properly_intersected (piece))
	{
	  return false;
	}

      // if the MR is equal to the given piece, return true.
      // MR will be eliminated.

      if (rectangle == piece)
	{
	  return true;
	}

      // =======================================================================
      // if there is a maximal rectangle to the left of the piece, create the
      // rectangle and add it to the output iterator

      if (rectangle.get_min_x () < piece.get_min_x ()
	  && is_maximal (rectangle.get_top ().begin (),
			 rectangle.get_top ().end (), piece.get_min_x (),
			 less<uint> ())
	  && is_maximal (rectangle.get_bottom ().begin (),
			 rectangle.get_bottom ().end (), piece.get_min_x (),
			 less<uint> ()))
	{
	  MAXIMAL_RECTANGLE left_rectangle (
	      rectangle.get_min_x (), rectangle.get_min_y (),
	      piece.get_min_x () - rectangle.get_min_x (),
	      rectangle.get_height ());

	  out = std::move (left_rectangle);
	}

      // test if there is a maximal rectangle to the right of the piece

      if (rectangle.get_max_x () > piece.get_max_x ()
	  && is_maximal (rectangle.get_top ().rbegin (),
			 rectangle.get_top ().rend (), piece.get_max_x (),
			 greater<uint> ())
	  && is_maximal (rectangle.get_bottom ().rbegin (),
			 rectangle.get_bottom ().rend (), piece.get_max_x (),
			 greater<uint> ()))
	{
	  MAXIMAL_RECTANGLE right_rectangle (
	      piece.get_max_x (), rectangle.get_min_y (),
	      rectangle.get_max_x () - piece.get_max_x (),
	      rectangle.get_height ());
	  out = std::move (right_rectangle.update ());
	}

      // test if there is a maximal rectangle to the top of the piece

      if (rectangle.get_max_y () > piece.get_max_y ()
	  && is_maximal (rectangle.get_left ().rbegin (),
			 rectangle.get_left ().rend (), piece.get_max_y (),
			 greater<uint> ())
	  && is_maximal (rectangle.get_right ().rbegin (),
			 rectangle.get_right ().rend (), piece.get_max_y (),
			 greater<uint> ()))
	{
	  MAXIMAL_RECTANGLE top_rectangle (
	      rectangle.get_min_x (), piece.get_max_y (),
	      rectangle.get_length (),
	      rectangle.get_max_y () - piece.get_max_y ());
	  out = std::move (top_rectangle.update ());
	}

      // test if there is a maximal rectangle to the bottom of the piece

      if (rectangle.get_min_y () < piece.get_min_y ()
	  && is_maximal (rectangle.get_left ().begin (),
			 rectangle.get_left ().end (), piece.get_min_y (),
			 less<uint> ())
	  && is_maximal (rectangle.get_right ().begin (),
			 rectangle.get_right ().end (), piece.get_min_y (),
			 less<uint> ()))
	{
	  MAXIMAL_RECTANGLE bottom_rectangle (
	      rectangle.get_min_x (), rectangle.get_min_y (),
	      rectangle.get_length (),
	      piece.get_min_y () - rectangle.get_min_y ());

	  out = std::move (bottom_rectangle.update ());
	}
      return true;
    }
}

#endif /* update_MR_h */
