 /*!
 * \file difference_process_aux.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Mar 2, 2018
 */

#ifndef PLACE1PIECE_FUNCS_HPP_
#define PLACE1PIECE_FUNCS_HPP_

#include <algorithm>
#include <iterator>
#include <vector>
#include <tuple>

#include "../algorithms/placement_positions.hpp"
#include "../algorithms/place_piece_update_mr.hpp"
#include "../model/maximal_rectangle.hpp"
#include "../model/prototype.hpp"
#include "../model/piece.hpp"


namespace two_dim_skp
{
  // comparison function to compare MAXIMAL_RECTANGLEs by ratio
  // (perimeter / adjacent_perimeter)
  //

  bool
  increasing_ratio (const MAXIMAL_RECTANGLE& a, const MAXIMAL_RECTANGLE& b);

  //

  bool
  compare_induced_rectangles (const std::vector<PLACEABLE_RECTANGLE>& a,
			      const std::vector<PLACEABLE_RECTANGLE>& b);

  //
  //

  template<template<typename, typename ...> class Container>
  std::vector<PLACEABLE_RECTANGLE>
  compute_induced_mr (
      const Container<MAXIMAL_RECTANGLE>& maximal_rectangles,
      const PLACEABLE_RECTANGLE& piece)
  {
    // the output vector

    vector<PLACEABLE_RECTANGLE> induced_mr;

    // test all MAXIMAL_RECTANGLEs and split those that intersect the given
    // PLACEABLE_RECTANGLE

    for (auto& maximal_rectangle : maximal_rectangles)
      {
	// There is no intersection. Just add the MAXIMAL_RECTANGLE to the
	// output vector.

	if (!maximal_rectangle.is_properly_intersected (piece))
	  {
	    induced_mr.push_back(maximal_rectangle);
	    continue;
	  }

	// The MAXIMAL_RECTANGLE is equal to the piece, do nothing

	if (maximal_rectangle == piece)
	  {
	    continue;
	  }

	// The MAXIMAL_RECTANGLE intersects the piece. Check if there are
	// induced MAXIMAL_RECTANGLEs to the left of the piece.

	if (maximal_rectangle.get_min_x () < piece.get_min_x ()
	    && is_maximal (maximal_rectangle.get_top ().begin (),
			   maximal_rectangle.get_top ().end (),
			   piece.get_min_x (),
			   less<uint> ())
            && is_maximal (maximal_rectangle.get_bottom ().begin (),
			   maximal_rectangle.get_bottom ().end (),
			   piece.get_min_x (),
			   less<uint> ()))
	  {
	    PLACEABLE_RECTANGLE induced_rectangle (
		maximal_rectangle.get_min_x (),
		maximal_rectangle.get_min_y (),
		piece.get_min_x () - maximal_rectangle.get_min_x (),
		maximal_rectangle.get_height ());

	    induced_mr.push_back(std::move (induced_rectangle));
	  }

	// Induced MAXIMAL_RECTANGLEs to the right of the piece

	if (maximal_rectangle.get_max_x () > piece.get_max_x ()
	    && is_maximal (maximal_rectangle.get_top ().rbegin (),
			   maximal_rectangle.get_top ().rend (),
			   piece.get_max_x (),
			   greater<uint> ())
            && is_maximal (maximal_rectangle.get_bottom ().rbegin (),
			   maximal_rectangle.get_bottom ().rend (),
			   piece.get_max_x (),
			   greater<uint> ()))
	  {
	    PLACEABLE_RECTANGLE induced_rectangle (
		piece.get_max_x (),
		maximal_rectangle.get_min_y (),
		maximal_rectangle.get_max_x () - piece.get_max_x (),
		maximal_rectangle.get_height ());
	    induced_mr.push_back(std::move (induced_rectangle));
	  }

	// Induced MAXIMAL_RECTANGLEs to the top of the piece

	if (maximal_rectangle.get_max_y () > piece.get_max_y ()
	    && is_maximal (maximal_rectangle.get_left ().rbegin (),
			   maximal_rectangle.get_left ().rend (),
			   piece.get_max_y (),
			   greater<uint> ())
	    && is_maximal (maximal_rectangle.get_right ().rbegin (),
			   maximal_rectangle.get_right ().rend (),
			   piece.get_max_y (),
			   greater<uint> ()))
	  {
	    PLACEABLE_RECTANGLE induced_rectangle (
		maximal_rectangle.get_min_x (),
		piece.get_max_y (),
		maximal_rectangle.get_length (),
		maximal_rectangle.get_max_y () - piece.get_max_y ());
	    induced_mr.push_back(std::move (induced_rectangle));
	  }

	// Induced MAXIMAL_RECTANGLEs to the bottom of the piece

	if (maximal_rectangle.get_min_y () < piece.get_min_y ()
	    && is_maximal (maximal_rectangle.get_left ().begin (),
			   maximal_rectangle.get_left ().end (),
			   piece.get_min_y (),
			   less<uint> ())
	    && is_maximal (maximal_rectangle.get_right ().begin (),
			   maximal_rectangle.get_right ().end (),
			   piece.get_min_y (),
			   less<uint> ()))
	  {
	    PLACEABLE_RECTANGLE induced_rectangle (
		maximal_rectangle.get_min_x (),
		maximal_rectangle.get_min_y (),
		maximal_rectangle.get_length (),
		piece.get_min_y () - maximal_rectangle.get_min_y ());

	    induced_mr.push_back(std::move (induced_rectangle));
	  }
      }

    return std::move (induced_mr);
  }

  /// Places a PIECE of the given PROTOTYPE in the MAXIMAL_RECTANGLE with
  /// smallest area among those that fit the PROTOTYPE.
  ///
  ///
  /// -
  /// - From the four possible placements within the MAXIMAL_RECTANGLE, the
  ///   PIECE is placed on the highest in the lexicographical order of the
  ///   vectors formed by the areas of the induced MAXIMAL_RECTANGLEs in
  ///   decreasing order. Ties in the vector are solved in increasing order of
  ///   the Y-coordinates of the bottom-left corner of the MAXIMAL_RECTANGLEs.
  ///
  /// - In case of ties, the PIECE is placed in the corner where the lowest
  ///   MAXIMAL_RECTANGLE has the biggest area.
  ///
  template<template<typename, typename ...> class Container>
  PIECE
  place (const Container<MAXIMAL_RECTANGLE>& maximal_rectangles,
	 PROTOTYPE& proto)
  {
    //
    // if there is a single maximal rectangle, default to bottom-left placement
    //

    if (maximal_rectangles.size () == 1)
      {
	const auto mr = maximal_rectangles.begin ();
	return std::move (proto.place (mr->get_min_x (), mr->get_min_y ()));
      }


    //
    // find the rectangle with minimum area fitting the prototype
    //

    // get the first rectangle fitting the prototype

    const auto end = maximal_rectangles.end();
    auto max =
	std::find_if (
	    maximal_rectangles.begin(),
	    end,
	    [&proto] (const MAXIMAL_RECTANGLE& rect)
	    {
	      return (rect.get_length () >= proto.get_length ())
		  && (rect.get_height () >= proto.get_height ());
	    });

    // find the rectangle with minimum area fitting the prototype

    for (auto it = std::next(max); it != end; ++it)
      {
	auto& mr = *it;
	if (mr.get_length () >= proto.get_length ()
	    && mr.get_height() >= proto.get_height ()
	    && mr.get_area() < max->get_area())
	  {
	    max = it;
	  }
      }


    //
    // find the best placement in the selected rectangle
    //

    const MAXIMAL_RECTANGLE& maximal_rectangle = *max;

    typedef std::tuple<uint, uint, std::vector<PLACEABLE_RECTANGLE>> PLACEMENT;
    std::vector<PLACEMENT> placements;

    // Bottom-left corner

    {
      PLACEMENT bottom_left;

      std::get<0> (bottom_left) = maximal_rectangle.get_min_x ();
      std::get<1> (bottom_left) = maximal_rectangle.get_min_y ();
      std::get<2> (bottom_left) = compute_induced_mr (
	  maximal_rectangles,
	  PLACEABLE_RECTANGLE (std::get<0> (bottom_left),
			       std::get<1> (bottom_left),
			       proto.get_length (), proto.get_height ()));

      placements.push_back(std::move (bottom_left));
    }

    // Bottom-right corner. Tested only if the piece length is smaller than the
    // MAXIMAL_RECTANGLE's length

    if (proto.get_length () != maximal_rectangle.get_length ())
      {
	PLACEMENT bottom_right;

	std::get<0> (bottom_right) = maximal_rectangle.get_max_x () - proto.get_length ();
	std::get<1> (bottom_right) = maximal_rectangle.get_min_y ();
	std::get<2> (bottom_right) = compute_induced_mr (
	    maximal_rectangles,
	    PLACEABLE_RECTANGLE (std::get<0> (bottom_right),
				 std::get<1> (bottom_right),
				 proto.get_length (), proto.get_height ()));

	placements.push_back(std::move (bottom_right));
      }

    // Top-left corner

    {
      PLACEMENT top_left;

      std::get<0> (top_left) = maximal_rectangle.get_min_x ();
      std::get<1> (top_left) = maximal_rectangle.get_max_y () - proto.get_height ();
      std::get<2> (top_left) = compute_induced_mr (
	  maximal_rectangles,
	  PLACEABLE_RECTANGLE (std::get<0> (top_left),
			       std::get<1> (top_left),
			       proto.get_length (), proto.get_height ()));

      placements.push_back(std::move (top_left));
    }

    // Top-right corner. Tested only if the piece height is smaller than the
    // MAXIMAL_RECTANGLE's width

    if (proto.get_height () != maximal_rectangle.get_height ())
      {
   	PLACEMENT top_right;

   	std::get<0> (top_right) = maximal_rectangle.get_max_x () - proto.get_length();
   	std::get<1> (top_right) = maximal_rectangle.get_max_y () - proto.get_height();
   	std::get<2> (top_right) = compute_induced_mr (
   	    maximal_rectangles,
   	    PLACEABLE_RECTANGLE (std::get<0> (top_right),
   				 std::get<1> (top_right),
   				 proto.get_length (), proto.get_height ()));

   	placements.push_back(std::move (top_right));
      }

    auto best_placement = std::max_element(
	placements.begin (),
	placements.end (),
	[] (auto& a, auto& b)
	{
	  return compare_induced_rectangles(std::get<2> (a), std::get<2> (b));
	});

    //
    // place and return the PIECE
    //

    return std::move (
	proto.place (std::get<0> (*best_placement),
		     std::get<1> (*best_placement)));
  }

} /* namespace two_dim_skp */

#endif /* PLACE1PIECE_FUNCS_HPP_ */
