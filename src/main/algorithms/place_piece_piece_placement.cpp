/*!
 * \file place_piece_piece_placement.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Dec 26, 2018
 */

#include "place_piece_piece_placement.hpp"

#include <algorithm>
#include <numeric>
#include <limits>

#include "../types.hpp"

namespace two_dim_skp
{
  bool
  increasing_ratio (const MAXIMAL_RECTANGLE& a, const MAXIMAL_RECTANGLE& b)
  {
    return a.get_adjacent_perimeter () * b.get_perimeter()
	< a.get_perimeter () * b.get_adjacent_perimeter ();
  }

  bool
  compare_induced_rectangles (const std::vector<PLACEABLE_RECTANGLE>& a,
  			      const std::vector<PLACEABLE_RECTANGLE>& b)
  {
    // compare PLACEABLE_RECTANGLEs by area

    auto increasing_area = [] (
	const PLACEABLE_RECTANGLE& r_a, const PLACEABLE_RECTANGLE& r_b)
	{
	  return r_a.get_area () < r_b.get_area ();
	};

    auto decreasing_area = [] (
	const PLACEABLE_RECTANGLE& r_a, const PLACEABLE_RECTANGLE& r_b)
	{
	  return r_a.get_area () > r_b.get_area ();
	};

    // compare PLACEABLE_RECTANGLEs by increasing y-coordinate of the
    // bottom-left corner

    auto increasing_min_y = [] (
	const PLACEABLE_RECTANGLE& r_a, const PLACEABLE_RECTANGLE& r_b)
	{
	  return r_a.get_min_y () < r_b.get_min_y ();
	};

    // compare PLACEABLE_RECTANGLEs by decreasing area while solving ties by
    // increasing y-coordinate of the bottom-left corner

    auto decreasing_area__increasing_min_y = [] (
	const PLACEABLE_RECTANGLE& r_a, const PLACEABLE_RECTANGLE& r_b)
    	{
	  uint a_area = r_a.get_area ();
	  uint b_area = r_b.get_area ();

	  return (a_area > b_area) || ((a_area == b_area) &&
	      (r_a.get_min_y () < r_b.get_min_y ()));
    	};

    // Sort the vectors by decreasing area. Ties are solved using the
    // increasing order of the y-coordinate of their bottom-left corner.

    std::vector<PLACEABLE_RECTANGLE> sorted_a (a);
    std::sort(sorted_a.begin (), sorted_a.end (),
	      decreasing_area__increasing_min_y);

    std::vector<PLACEABLE_RECTANGLE> sorted_b (b);
    std::sort(sorted_b.begin (), sorted_b.end (),
	      decreasing_area__increasing_min_y);


    // if the vectors are different, use lexicographical comparison

    if (!std::equal(
	sorted_a.begin (), sorted_a.end (),
	sorted_b.begin (), sorted_b.end (),
	[] (auto& r_a, auto& r_b)
	{
	  return r_a.get_area () == r_b.get_area ();
	}))
      {
	return std::lexicographical_compare (sorted_a.begin (), sorted_a.end (),
					     sorted_b.begin (), sorted_b.end (),
					     increasing_area);
      }

    // if the vectors are equal, solve the tie with the lexicographical
    // comparison area of the lowest rectangle of each vector

    uint a_lowest_coord = std::min_element(a.begin(), a.end(),
					   increasing_min_y)->get_min_y ();
    std::vector <PLACEABLE_RECTANGLE> a_lowest;
    std::for_each(a.begin (), a.end (),
		  [&a_lowest_coord, &a_lowest] (auto& el)
		  {
		    if (el.get_min_y () == a_lowest_coord)
		      {
			a_lowest.push_back(el);
		      }
		  });
    std::sort(a_lowest.begin (), a_lowest.end (), decreasing_area);

    uint b_lowest_coord = std::min_element (b.begin (), b.end (),
					    increasing_min_y)->get_min_y ();
    std::vector <PLACEABLE_RECTANGLE> b_lowest;
    std::for_each(b.begin (), b.end (),
		  [&b_lowest_coord, &b_lowest] (auto& el)
    		  {
    		    if (el.get_min_y () == b_lowest_coord)
    		      {
    			b_lowest.push_back(el);
    		      }
    		  });
    std::sort (b_lowest.begin (), b_lowest.end (), decreasing_area);


    return std::lexicographical_compare (a_lowest.begin (), a_lowest.end (),
					 b_lowest.begin (), b_lowest.end (),
					 increasing_area);
  }
}
