/*
 * BeamSearch.hpp
 *
 *  Created on: May 30, 2017
 *      Author: calegria
 */

#ifndef BS_BEAM_SEARCH_HPP_
#define BS_BEAM_SEARCH_HPP_

#include <functional>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <utility>
#include <string>
#include <queue>
#include <tuple>

#include <boost/graph/adjacency_list.hpp>
#include <boost/property_map/property_map.hpp>
using namespace boost;

#include "beam_search_tree.hpp"

namespace bs
{
  /// Node concept
  ///
  /// The Node used in the search tree. It has to be Default Constructible.


  /// Filter Concept
  ///
  /// The concept defining the callbacks Beam Search is going to use during the
  /// algorithm.
  ///
  ///
  /// Public data type declarations
  ///
  /// NODE_TYPE - The type of the node used to construct the search tree. The
  ///             type must be a model of the Node Concept (more details below).
  ///
  /// GLOBAL_EVAL_TYPE - The type returned by the method global_evaluation.
  ///
  /// LOCAL_EVAL_TYPE - The type returned by the method local_evaluation.
  ///
  ///
  /// Methods
  ///
  /// uint get_filter_width() const;
  ///
  /// Returns the parameter \alpha of Beam Search: the maximum out-degree of the
  /// search tree.
  ///
  /// uint get_beam_width() const;
  ///
  /// Returns the parameter \beta of Beam Search: the maximum number of nodes by
  /// level with out-degree greater than zero.
  ///
  /// const NODE_TYPE& get_root_node() const;
  ///
  /// The node to be used as the root of the search tree. Every node returned by
  /// filter_nodes should not be equal from the root node.
  ///
  /// template<class OutIterator>
  /// void
  /// local_evaluation (const node_type& parent, OutIterator out) const;
  ///
  /// Return the best \alpha nodes spanned from the node "parent". The nodes
  /// should be written to the output iterator "out".
  ///
  ///
  /// eval_type
  /// global_evaluation(node_type& node) const;
  ///
  /// Perform the global evaluation on the given node. Return the result of the
  /// evaluation.

  /// The Beam Search method
  ///
  /// TODO decide where to put the pruning mechanism for both local and global
  ///      evaluations.
  ///
  /// TODO resolve ties in global evaluation
  ///
  template<class BSFilter>
    BEAM_SEARCH_TREE<
	typename BSFilter::NODE_TYPE,
	typename BSFilter::LOCAL_EVAL_TYPE,
	typename BSFilter::GLOBAL_EVAL_TYPE>
    beam_search (BSFilter& filter)
    {
      //
      // data types
      //

      typedef BEAM_SEARCH_TREE<
		typename BSFilter::NODE_TYPE,
		typename BSFilter::LOCAL_EVAL_TYPE,
		typename BSFilter::GLOBAL_EVAL_TYPE> TREE;
      typedef typename BSFilter::NODE_TYPE NODE;
      typedef typename TREE::VERTEX VERTEX;

      // the vectors containing the locally filtered nodes

      typedef std::vector< std::pair< NODE *, std::vector<NODE> > > FILTER_FRONT;


      //
      // initialization
      //

      // wrapping object for the search tree

      const auto& root_node = filter.get_root_node ();
      TREE bs_tree (
	  root_node,
	  std::move (filter.local_evaluation (root_node)),
	  std::move (filter.global_evaluation (root_node)));

      // size of the array of leaf vertices

      const size_t beam_front_size = filter.get_beam_width ();

      // the array of leaf vertices (initialized with the root vertex)

      std::vector<VERTEX> beam_front = { bs_tree.root };


      //
      // main cycle
      //

      while (!beam_front.empty ())
	{
	  // setup the locally filtered nodes

	  FILTER_FRONT filter_front;
	  std::transform (
	      beam_front.begin (),
	      beam_front.end (),
	      std::back_inserter (filter_front),
	      [&bs_tree] (VERTEX& v)
	      {
		NODE *node = &(bs_tree.node_map[v]);
		return std::move(std::make_pair(node, std::vector<NODE> ()));
	      });

	  filter.create_nodes (filter_front);

	  // populate tree

	  std::vector<VERTEX> best_children;
	  auto f_it = filter_front.begin ();

	  for (auto& parent_v : beam_front)
	    {
	      for (auto& child : (f_it++)->second)
		{
		  // create child vertex
		  VERTEX child_v = add_vertex (bs_tree.tree);
		  best_children.push_back (child_v);

		  // create edge from parent to child
		  add_edge (parent_v, child_v, bs_tree.tree);

		  // add local evaluation
		  auto local_eval = filter.local_evaluation (child);
		  boost::put (bs_tree.local_eval_map,
			      child_v, std::move (local_eval));

		  // add global evaluation
		  auto global_eval = filter.global_evaluation (child);
		  boost::put (bs_tree.global_eval_map,
			      child_v, std::move (global_eval));

		  // add node
		  boost::put (bs_tree.node_map, child_v, std::move (child));
		}
	    }

	  // create new beam_front

	  typename decltype(best_children)::iterator start_it;
	  if (best_children.size () > beam_front_size)
	    {
	      // if there are more than \beta children, first sort
	      // the array in decreasing order

	      std::sort(best_children.begin (), best_children.end (),
	    		    [&filter, &bs_tree] (VERTEX& a, VERTEX& b)
	    		    {
	    		      return filter.compare (
	    			  bs_tree.global_eval_map[a],
				  bs_tree.global_eval_map[b]);
	    		    });

	      // and then select the best \beta children

	      start_it = best_children.begin();
	      std::advance (start_it, best_children.size () - beam_front_size);
	    }
	  else
	    {
	      // if there are less than \beta children, select all the children
	      // from the array

	      start_it = best_children.begin ();
	    }

	  // update beam front with the best children

	  beam_front.clear ();
	  std::move (start_it, best_children.end (),
		     std::back_inserter (beam_front));
	}


      //
      // return result
      //

      bs_tree.find_solutions ();
      return std::move(bs_tree);
    }
}

#endif /* BS_BEAM_SEARCH_HPP_ */
