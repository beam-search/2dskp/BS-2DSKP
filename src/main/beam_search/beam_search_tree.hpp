/*!
 * \file beam_search_tree.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Oct 14, 2018
 */

#ifndef BEAM_SEARCH_TREE_HPP_
#define BEAM_SEARCH_TREE_HPP_

#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/breadth_first_search.hpp>
using namespace boost;

namespace bs
{
  // properties associated to each vertex in the beam search tree

  // node

  struct NODE_T
  {
    typedef vertex_property_tag kind;
  };

  // local evaluation of a node

  struct LOCAL_EVAL_T
  {
    typedef vertex_property_tag kind;
  };

  // global evaluation of a node

  struct GLOBAL_EVAL_T
  {
    typedef vertex_property_tag kind;
  };


  /// Visitor Concept
  ///
  /// A concept defining a callback used while traversing the result search
  /// tree. There is only one method in this concept. This method is called when
  /// a vertex on the tree search is visited during the traversal algorithm.
  ///
  /// void
  /// visit_node (const NODE& node,
  ///		  unsigned long vertex_id,
  ///		  const LOCAL_EVAL& local_eval,
  ///		  const GLOBAL_EVAL& global_eval)
  ///
  /// Where:
  /// - NODE, LOCAL_EVAL, and GLOBAL_EVAL are the types of the visited node, its
  ///   local evaluation, and its global evaluation
  /// - node is a reference to the node being visited
  /// - vertex_id is the unique identifier of node
  /// - local_eval is the local evaluation of node
  /// - global_eval is the global evaluation of node

  /// Wrapper visitor for BFS traversals

  template<class BSVisitor>
    class BFS_VISITOR : public default_bfs_visitor
    {
    public:
      BFS_VISITOR (BSVisitor& visitor) :
	  visitor (visitor)
      {
      }

      template<typename Vertex, typename Graph>
	void
	discover_vertex (Vertex u, const Graph& g) const
	{
	  const auto& node_map = get (NODE_T (), g);
	  const auto& local_eval_map = get (LOCAL_EVAL_T (), g);
	  const auto& global_eval_map = get (GLOBAL_EVAL_T (), g);

	  visitor.visit_node (node_map[u],
			      (unsigned long) u,
			      local_eval_map[u],
			      global_eval_map[u]);
	}

    private:
      BSVisitor& visitor;
    };

  /// Wrapper visitor for DFS traversals

  template<class BSVisitor>
    class DFS_VISITOR : public default_dfs_visitor
    {
    public:
      DFS_VISITOR (BSVisitor& visitor) :
	  visitor (visitor)
      {
      }

      template<typename Vertex, typename Graph>
	void
	discover_vertex (Vertex u, const Graph& g) const
	{
	  const auto& node_map = get (NODE_T (), g);
	  const auto& local_eval_map = get (LOCAL_EVAL_T (), g);
	  const auto& global_eval_map = get (GLOBAL_EVAL_T (), g);

	  visitor.visit_node (node_map[u],
			      (unsigned long) u,
			      local_eval_map[u],
			      global_eval_map[u]);
	}

    private:
      BSVisitor& visitor;
    };


  /// Beam Search Tree
  ///
  template<class NODE, typename LOCAL_EVALUATION, typename GLOBAL_EVALUATION>
  class BEAM_SEARCH_TREE
  {
      template<class BSFilter>
	friend BEAM_SEARCH_TREE<
	    typename BSFilter::NODE_TYPE,
	    typename BSFilter::LOCAL_EVAL_TYPE,
	    typename BSFilter::GLOBAL_EVAL_TYPE>
	beam_search (BSFilter&);

  private:

      //
      // data types
      //

      typedef NODE_T NODE_PROPERTY;
      typedef LOCAL_EVAL_T LOCAL_EVAL_PROPERTY;
      typedef GLOBAL_EVAL_T GLOBAL_EVAL_PROPERTY;

      typedef property<NODE_PROPERTY, NODE,
	      property<LOCAL_EVAL_PROPERTY, LOCAL_EVALUATION,
	      property<GLOBAL_EVAL_PROPERTY, GLOBAL_EVALUATION > > > VERTEX_PROPERTIES;

      typedef adjacency_list<listS, vecS, bidirectionalS, VERTEX_PROPERTIES> TREE;
      typedef typename property_map<TREE, NODE_PROPERTY>::type NODE_MAP;
      typedef typename property_map<TREE, LOCAL_EVAL_PROPERTY>::type LOCAL_EVAL_MAP;
      typedef typename property_map<TREE, GLOBAL_EVAL_PROPERTY>::type GLOBAL_EVAL_MAP;

      typedef typename graph_traits<TREE>::vertex_descriptor VERTEX;
      typedef typename graph_traits<TREE>::vertex_iterator VERTEX_IT;

      typedef typename graph_traits<TREE>::in_edge_iterator IN_EDGE_IT;
      typedef typename graph_traits<TREE>::out_edge_iterator OUT_EDGE_IT;


      TREE tree;
      VERTEX root;

      NODE_MAP node_map;
      LOCAL_EVAL_MAP local_eval_map;
      GLOBAL_EVAL_MAP global_eval_map;

      bool same_branch;
      uint solutions;
      VERTEX tree_solution;
      VERTEX global_solution;

      // obtain statistics from the current tree

      void
      find_solutions ()
      {
	//
	// set solutions
	//

	VERTEX_IT v_it, last_v;
	for (std::tie (v_it, last_v) = vertices (this->tree); v_it != last_v;
	    ++v_it)
	  {
	    auto& node_current = node_map[*v_it];

	    // update global solution

	    auto& node_global = node_map[global_solution];
	    if (node_current.get_profit () >= node_global.get_profit ())
	      {
		global_solution = *v_it;
	      }

	    // update tree solution if current vertex is a leaf

	    OUT_EDGE_IT first_e, last_e;
	    std::tie (first_e, last_e) = out_edges (*v_it, this->tree);//Devuelve iteradores a inicio y fin de aristas salientes

	    if (first_e == last_e)//Si son iguales, es que no sale ninguna arista (nodo hoja).
	      {
		auto& node_tree = node_map[tree_solution];
		if (node_current.get_profit () >= node_tree.get_profit ())
		  {
		    tree_solution = *v_it;
		  }
	      }
	  }

	//
	// find if there are several best solutions
	//

	{
	  auto& node_global = node_map[this->global_solution];
	  std::tie (v_it, last_v) = vertices (this->tree);
	  solutions = std::count_if (
	      v_it, last_v,
	      [&node_global, this] (const VERTEX v)
	      {
		return this->node_map[v].get_profit () == node_global.get_profit ();
	      });
	}

	//
	// find if tree solution and global solutions belong to the same branch
	//

	VERTEX current = tree_solution;
	while (current != root)
	  {
	    if (global_solution == current)
	      {
		same_branch = true;
		break;
	      }

	    IN_EDGE_IT first, last;
	    std::tie (first, last) = in_edges (current, this->tree);
	    current = source (*first, this->tree);
	  }
      }

  public:

      BEAM_SEARCH_TREE (const NODE& root_node,
			LOCAL_EVALUATION&& local_evaluation,
			GLOBAL_EVALUATION&& global_evaluation)
      {
	this->node_map = get (NODE_PROPERTY (), this->tree);
	this->local_eval_map = get (LOCAL_EVAL_PROPERTY (), this->tree);
	this->global_eval_map = get (GLOBAL_EVAL_PROPERTY (), this->tree);

	this->root = add_vertex (this->tree);
	boost::put (this->node_map, this->root, root_node);
	boost::put (this->local_eval_map, this->root, std::move (local_evaluation));
	boost::put (this->global_eval_map, this->root, std::move (global_evaluation));

	this->same_branch = false;
	this->solutions = 1;
	this->tree_solution = this->global_solution = this->root;
      }

      virtual
      ~BEAM_SEARCH_TREE ()
      {
      }

      //
      // move semantics
      //

      BEAM_SEARCH_TREE (BEAM_SEARCH_TREE&& beam_search_tree) :
	tree (beam_search_tree.tree),
	root (std::move(beam_search_tree.root)),
	node_map (get (NODE_PROPERTY (), this->tree)),
	local_eval_map (get (LOCAL_EVAL_PROPERTY (), this->tree)),
	global_eval_map (get (GLOBAL_EVAL_PROPERTY (), this->tree)),
	same_branch (std::exchange(beam_search_tree.same_branch, false)),
	solutions (std::exchange(beam_search_tree.solutions, 0)),
	tree_solution (std::move(beam_search_tree.tree_solution)),
	global_solution (std::move(beam_search_tree.global_solution))
      {
      }
      BEAM_SEARCH_TREE&
      operator= (BEAM_SEARCH_TREE&&) = default;

      /// get the best leaf of the tree
      ///
      const NODE&
      get_tree_solution () const
      {
	const NODE& node = this->node_map[this->tree_solution];
	return node;
      }

      /// get the local evaluation of the best leaf of the tree
      ///
      const LOCAL_EVALUATION&
      get_tree_local_evaluation () const
      {
      	const LOCAL_EVALUATION& eval = this->local_eval_map[this->tree_solution];
      	return eval;
      }

      /// get the local evaluation of the best leaf of the tree
      ///
      const GLOBAL_EVALUATION&
      get_tree_global_evaluation () const
      {
	const GLOBAL_EVALUATION& eval =
	    this->global_eval_map[this->tree_solution];
	return eval;
      }

      /// get the best node of the tree
      ///
      const NODE&
      get_global_solution () const
      {
	const NODE& node = this->node_map[this->global_solution];
	return node;
      }

      /// get the local evaluation of the best leaf of the tree
      ///
      const LOCAL_EVALUATION&
      get_global_local_evaluation () const
      {
	const LOCAL_EVALUATION& eval =
	    this->local_eval_map[this->global_solution];
      	return eval;
      }

      /// get the local evaluation of the best leaf of the tree
      ///
      const GLOBAL_EVALUATION&
      get_global_global_evaluation () const
      {
	const GLOBAL_EVALUATION& eval =
	    this->global_eval_map[this->global_solution];
	return eval;
      }

      /// True if the best leaf and the best node are in the same branch of the
      /// tree
      ///
      bool
      is_same_branch () const
      {
	return this->same_branch;
      }

      /// True if the beast leaf and the best node are the same
      ///
      bool
      is_solution () const
      {
	return this->tree_solution == this->global_solution;
      }

      ///
      ///
      uint
      get_solutions () const
      {
	return this->solutions;
      }

      /// Traverse the tree breadth first
      ///
      /// Traverse the tree breadth first while calling the visit_node method on
      /// the given visitor.
      ///
      template<class BSVisitor>
	void
	breadth_first_traversal (BSVisitor& bs_visitor)
	{
	  BFS_VISITOR<BSVisitor> bfs_visitor (bs_visitor);
	  breadth_first_search (this->tree, this->root, visitor (bfs_visitor));
	}

      /// Traverse the tree depth first
      ///
      /// Traverse the tree depth first while calling the visit_node method on the
      /// given visitor.
      ///
      template<class BSVisitor>
	void
	depth_first_traversal (BSVisitor& bs_visitor)
	{
	  DFS_VISITOR<BSVisitor> dfs_visitor (bs_visitor);
	  depth_first_search (this->tree, visitor (dfs_visitor));
	}
  };

} /* namespace bs */

#endif /* BEAM_SEARCH_TREE_HPP_ */
