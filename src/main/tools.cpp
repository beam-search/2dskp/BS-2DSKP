/*!
 * \file two_dim_skp.cpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 6, 2017
 */

#include "tools.hpp"

#include <algorithm>
#include <stdexcept>
#include <numeric>
#include <utility>
#include <string>
#include <vector>

#include "model/piece.hpp"

#define DIM_WIDTH  0
#define DIM_HEIGHT 1

#define FIELDS_NUMBER	 5
#define FIELD_ID         0
#define FIELD_LENGTH	 1
#define FIELD_HEIGHT     2
#define FIELD_PROFIT     3
#define FIELD_MAX_COPIES 4

namespace two_dim_skp
{

  void
  readFrom2KPFile (std::ifstream& is, uint& length, uint& height,
		   PROTOTYPES& prototypes)
  {
    int line = 1;
    std::string header;

    //
    // bin dimensions
    //

    uint dimensions[2];

    if (!(is >> header) || header != "dim,")
      {
	throw std::runtime_error (
	    "line " + std::to_string (line) + ": line header 'dim' expected");
      }

    is.putback (',');
    std::for_each (
	dimensions, dimensions + 2,
	[&is, line] (uint &number)
	{
	  if (!is.ignore() || !(is >> number) || is.eof())
	    {
	      throw std::runtime_error(
		  "line " + std::to_string(line) + ": integer number expected");
	    }
	});

    length = dimensions[DIM_WIDTH];
    height = dimensions[DIM_HEIGHT];

    //
    // pieces
    //

    uint fields[FIELDS_NUMBER];

    while (!is.eof ())
      {
	++line;

	// line header

	is >> header;
	if (is.eof ())
	  {
	    break;
	  }
	if (!is || (header != "rect,"))
	  {
	    throw std::runtime_error (
		"line " + std::to_string (line) +
		": line header 'rect' expected");
	  }

	is.putback (',');
	std::for_each (
	    fields, fields + 5,
	    [&is, line] (uint &number)
	    {
	      if (!is.ignore() || !(is >> number))
		{
		  throw std::runtime_error(
		      "line " + std::to_string(line) +
		      ": integer number expected ");
		}
	    });

	auto result = prototypes.emplace (
	    std::make_pair (
		fields[FIELD_ID],
		PROTOTYPE (fields[FIELD_ID],
			   fields[FIELD_LENGTH], fields[FIELD_HEIGHT],
			   fields[FIELD_PROFIT], fields[FIELD_MAX_COPIES])));

	// Finds an element with the same id (key).

	if (!result.second)
	  {
	    throw std::runtime_error (
		"line " + std::to_string (line) + ": repeated prototype id '" +
		std::to_string (fields[FIELD_ID]) + "'");
	  }
      }

    if (line == 1)
      {
	throw std::runtime_error ("At least one piece is expected");
      }
  }

  void
  writeToSolFile (std::ofstream& os, uint scale, uint bin_width,
		  uint bin_height, const BIN& pieces)
  {
    bin_width *= scale;
    bin_height *= scale;

    // page dimensions exceed bin dimensions by 5%

    float page_width = bin_width * 1.2;
    float page_height = bin_height * 1.2;

    // document header

    os << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
	<< std::endl;

    // main svg element

    os << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" "
	"width=\"" << page_width << "\" "
	"height=\"" << page_height << "\">"
	<< std::endl;

    // style for all elements

    os << "<style>" << std::endl
	<< " text { font-family: Verdana; font-size: 12pt; fill: black; }" << std::endl
	<< " #bin { fill: ivory; stroke: none; }" << std::endl
	<< " #bin_border { fill: none; stroke-width: 3; stroke: black; }" << std::endl
	<< " .piece { fill: gainsboro; stroke-width: 1; stroke: black; }" << std::endl
	<< " .label { dominant-baseline: middle; text-anchor: middle; }" << std::endl
	<< " .coordinate { text-anchor: start; font-size: 8pt;}" << std::endl
	<< "</style>" << std::endl;

    // group elements so they can be transformed together

    os << "<g transform=\"translate("
	<< (page_width - bin_width) / 2 << " "
	<< (page_height - bin_height) / 2 << ")\">"
	<< std::endl;

    // bin

    os << " <rect id=\"bin\" x=\"0\" y=\"0\" "
	"width=\"" << bin_width << "\" "
	"height=\"" << bin_height << "\"/>"
	<< std::endl;

    // pieces

    for (auto &piece : pieces)
      {
	uint x = scale * piece.get_min_x ();
	uint y = bin_height - scale * piece.get_max_y ();

	uint width = scale * piece.get_length ();
	uint height = scale * piece.get_height ();

	os << " <rect class=\"piece\" "
	    "x=\"" << x << "\" "
	    "y=\"" << y << "\" "
	    "width=\"" << width << "\" "
	    "height=\"" << height << "\"/>"
	    << std::endl;
      }

    // labels

    for (auto& piece : pieces)
      {
    	// id

	float x = ((float) scale)
	    * ((float) piece.get_min_x () + ((float) piece.get_length () / 2));
	float y = (float) bin_height
	    - ((float) scale)
	    * ((float) piece.get_min_y () + ((float) piece.get_height () / 2));

    	os << " <text class=\"label\" "
	    "x=\"" << x << "\" "
	    "y=\"" << y << "\">"
    	    << piece.get_id()
	    << "</text>"
    	    << std::endl;

    	// coordinates

	x = (float) scale * ((float) piece.get_min_x () + 0.2);
	y = (float) bin_height
	    - (float) scale * ((float) piece.get_min_y () + 0.2);

    	// coordinates

    	os << " <text class=\"coordinate\" "
    	    "x=\"" << x << "\" "
	    "y=\"" << y << "\">"
	    "(" << piece.get_min_x () << "," << piece.get_min_y () << ")"
	    "</text>"
	    << std::endl;
      }

    // total profit

    float x = bin_width / 2;
    float y = 1.05 * (float) bin_height;
    uint profit = accumulate (pieces.begin (), pieces.end (), 0,
			      [] (uint accumulator, const PIECE& p)
				{ return accumulator + p.get_profit();});

    os << " <text class=\"label\" "
	"x=\"" << x << "\" "
	"y=\"" << y << "\">"
	"Profit = " << profit << "</text>" << std::endl;

    // bin border

    os << " <rect id=\"bin_border\" x=\"0\" y=\"0\" "
	"width=\"" << bin_width << "\" height=\"" << bin_height << "\" />"
    	<< std::endl;

    // closing header

    os << "</g>" << std::endl << "</svg>";
  }

} /* namespace two_dim_skp */
