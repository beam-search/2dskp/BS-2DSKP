/*!
 * \file types.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 14, 2017
 */

#ifndef TYPES_HPP_
#define TYPES_HPP_

#include <unordered_map>
#include <vector>

namespace two_dim_skp
{
  // The unsigned integer used for bin and piece dimensions, and piece
  // coordinates.

  typedef unsigned int uint;

  // The map containing the prototypes loaded from an instance file.
  //
  // The key_type is an unsigned integer containing the id of the PROTOTYPE as
  // it is specified in the instance file. The mapped_type is the corresponding
  // PROTOTYPE.

  typedef std::unordered_map<uint, class PROTOTYPE> PROTOTYPES;

  // A bin containing a set of pieces already placed.

  typedef std::vector<class PIECE> BIN;
}

#endif /* TYPES_HPP_ */
