//
//  main.cpp
//  2DSKP-BestFit
//
//  Created by Marta Cabo on 29/11/17.
//  Copyright © 2017 Marta Cabo. All rights reserved.
//


#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <boost/filesystem.hpp>
namespace bfs = boost::filesystem;

#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <map>
#include <random>
#include <chrono>
#include <iomanip>

using namespace std;

#include "model/prototype.hpp"
#include "model/piece.hpp"
#include "tools.hpp"
#include "algorithms/best_fit.hpp"
#include "algorithms/place_piece.hpp"
#include "beam_search/beam_search.hpp"
#include "skp/skp_filter.hpp"


using namespace two_dim_skp;

// help option
//
#define OP_HELP  "help"
#define OPS_HELP "h"

// flag indicating whether to print verbose messages or not
//
#define OP_VERBOSE  "verbose"
#define OPS_VERBOSE "v"

// the name of the file containing the pieces information
//
#define OP_DATA "data"

// the text width when writing text to console
//
#define TEXT_WIDTH 80

string data_filename;

int
main (int argc, const char **argv)
{
  //
  // command line options setup
  //

  po::options_description desc ("Available options");
  desc.add_options () (OP_HELP "," OPS_HELP, "Print this help message") (
      OP_DATA, po::value<string> (&data_filename)->required (),
      "The *.2kp name of the file containing problem instance information") (
      OP_VERBOSE "," OPS_VERBOSE, "Print verbose messages");

  po::positional_options_description p_desc;
  p_desc.add (OP_DATA, -1);


  //
  // parsing options
  //

  po::variables_map vm;
  po::store (
      po::command_line_parser (argc, argv).options (desc).positional (p_desc).run (),
      vm);
  po::notify (vm);

  // check if 'help' options is requested, or a required option is missing
  //
  if (vm.count (OP_HELP) || !vm.count (OP_DATA))
    {
      cout << desc << endl;
      exit (EXIT_SUCCESS);
    }

  // reading problem instance from data file
  //
  uint width;
  uint height;
  PROTOTYPES prototypes;
  try
    {
      ifstream is (data_filename, ifstream::in);
      if (!is.is_open ())
	{
	  throw runtime_error (
	      "ERROR: Unable to open '" + data_filename + "' file");
	}

      readFrom2KPFile (is, width, height, prototypes);
      is.close ();
    }
  catch (std::exception& e)
    {
      cout << e.what () << endl;
      return EXIT_FAILURE;
    }


  //
  // defining visitor to print nodes
  //

  class print_visitor
  {
  public:
    print_visitor (uint length, uint height) :
	length (length), height (height)
    {
    }

    void
    visit_node (unsigned long node_id,
		const SKP_BS_FILTER::NODE_TYPE& node,
		const SKP_BS_FILTER::LOCAL_EVAL_TYPE& local_eval,
		const SKP_BS_FILTER::GLOBAL_EVAL_TYPE& global_eval)
    {
      string svg_file = "Tree/"
	  + bfs::path (data_filename).stem ().string ()
	  + std::to_string (node_id) + ".svg";

      ofstream os (svg_file);
      writeToSolFile (os, 30, length, height, node.get_bin ());
      os.close ();
    }

    uint length;
    uint height;
  };

  uint alpha = 4;
  uint beta = 6;

  SKP_BS_FILTER filter(std::move(prototypes), width, height, alpha, beta);
    
  //Start clock
  auto start = chrono::high_resolution_clock::now();
  //Start beam search
  auto result = bs::beam_search(filter);
    
  //Stop clock, and calculate difference
  auto end = chrono::high_resolution_clock::now();
  auto dur = end - start;
  auto ms = chrono::duration_cast<chrono::milliseconds>(dur).count();
  double time =(double)ms/1000;

  //==========================================================================
  //.stem() elimina la extensión del archivo .string() lo convierte a string.
  string svg_file = "BS_" + bfs::path (data_filename).stem ().string () + ".svg";

  SKP_BS_FILTER::GLOBAL_EVAL_TYPE global_eval =
      result.get_global_global_evaluation ();

  ofstream os (svg_file);
  writeToSolFile (os, 30, width, height, global_eval.get_bin ());
  os.close ();

  ofstream res_file ("Results.txt", ofstream::app);
  res_file << bfs::path (data_filename).stem ().string ()
      << "\t Solution: " << global_eval.get_profit ()
      << "\t Time(sec):\t" << fixed << setprecision (2) << time
      << "\t alpha: " << alpha << " beta: " << beta
      << "\t Is leaf: " << result.is_solution () << std::endl;
  res_file.close ();

  return EXIT_SUCCESS;
}
