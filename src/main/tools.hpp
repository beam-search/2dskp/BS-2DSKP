 /*!
 * \file tools.hpp
 *
 * \author Carlos Alegría Galicia
 * \date   Nov 6, 2017
 */

#ifndef TOOLS_HPP_
#define TOOLS_HPP_

#include <iostream>
#include <fstream>

#include "types.hpp"

namespace two_dim_skp
{
  /// Read an instance of the 2DSKP problem from a *.2kp data file
  ///
  void
  readFrom2KPFile (std::ifstream& is, uint& width, uint& height,
		   PROTOTYPES& prototypes);

  /// Print the given Piece or Prototype in *.svg format to the given ofstream
  ///
  void
  writeToSolFile (std::ofstream& os, uint scale, uint bin_width,
		  uint bin_height, const BIN& pieces);
}

#endif /* TOOLS_HPP_ */
