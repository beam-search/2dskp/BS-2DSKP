\documentclass[letterpaper,english]{article16}

% ========================================================================
% configuration
% ========================================================================

\usepackage[]{algorithm2e}
\usepackage{filecontents}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{mdframed}
\usepackage{listings}
\usepackage{xcolor}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}
%
\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\newcommand*{\Value}{\frac{1}{2}x^2}%

% The packages graphicx, amssymb, and amsmath are already loaded via
% the .cls file

% ========================================================================
% Macros and Definitions
% ========================================================================

% Add all additional macros here, do NOT include any additional files.

% The environments theorem (Theorem), invar (Invariant), lemma
% (Lemma), cor (Corollary), obs (Observation), conj (Conjecture), prop
% (Proposition), defini (Definition), and proof (Proof) are already
% defined in the .cls file. Add additional environments only if you
% REALLY need them.

\graphicspath{{fig/}{./fig/}}

\newcommand{\note}[1]{%
  \definecolor{notecolor}{hsb}{0.5, 0.7, 0.8}%
  \begin{mdframed}[
    leftline=true,%
    rightline=false,%
    topline=false,%
    bottomline=false,%
    linecolor=notecolor,%
    linewidth=8pt,%
    backgroundcolor=black!4%
    ]%
    \small%
    #1%
  \end{mdframed}%
}

\newcommand{\NODE}{\texttt{NODE\_TYPE}}
\newcommand{\TREE}{\texttt{TREE}}
\newcommand{\RECTANGLE}{\texttt{RECTANGLE}}
\newcommand{\PRECTANGLE}{\texttt{PLACEABLE\_RECTANGLE}}
\newcommand{\PIECE}{\texttt{PIECE}}
\newcommand{\PROTOTYPE}{\texttt{PROTOTYPE}}
\newcommand{\MRECTANGLE}{\texttt{MAXIMAL\_RECTANGLE}}
\newcommand{\FILTER}{\texttt{FILTER}}
\newcommand{\LOCAL}{\texttt{LOCAL\_EVAL\_TYPE}}
\newcommand{\GLOBAL}{\texttt{GLOBAL\_EVAL\_TYPE}}
\newcommand{\BST}{\texttt{BEAM\_SEARCH\_TREE}}
\newcommand{\SBF}{\texttt{SKP\_BS\_FILTER}}
\newcommand{\SNODE}{\texttt{SKP\_NODE}}
\newcommand{\BF}{\texttt{BEAM\_FRONT}}
\newcommand{\FF}{\texttt{FILTER\_FRONT}}

% ========================================================================
% Title
% ========================================================================

\title{Beam search implementation for the 2D Single Knapsack Problem}

% \author{Hamid Hoorfar \thanks{Department of Computer Engineering and
%     Information Technology, Amirkabir University of Technology (Tehran
%     Polytechnic), {\tt \{hoorfar,ar.bagheri\}@aut.ac.ir}}\and Alireza
%   Bagheri\footnotemark[1]~\footnote{\textit{Coresponding author}.}  }

% ========================================================================
% Content
% ========================================================================

\begin{document}

\maketitle

\begin{abstract}
  In this document we describe the implementation of the Beam Search
  approach to the 2D Single Knapsack Problem.
\end{abstract}

\section{Beam Search}

The Beam Search implementation is separated into two main
components. The \emph{Beam Search Framework} (BSF for short) contains
the problem-agnostic implementation of the Beam Search strategy. The
\emph{Concept Layer} contains the problem-specific user code used by
the BSF. The Concept Layer and the BSF interact with each other via
\href{https://en.wikipedia.org/wiki/Generic_programming}{Generic
  Programming}. The BSF defines the concept \FILTER{} that requires
declarations of node, local and global evaluation types, and functions
to create, evaluate, and compare nodes. The user code in the Concept
Layer must contain a class that model this concept.

The BSF is divided into two components: A tree structure called \BST{}
that stores the nodes along with their local and global evaluations,
and the search algorithm that constructs the tree and decides which
branches to expand. The entry point of the BSF is a function
\begin{lstlisting}[language=C++]
BEAM_SEARCH_TREE
beam_search(const FILTER&);
\end{lstlisting}
that returns the constructed tree. The \BST{} class provides methods
query for the solution or other properties.

\note{Most of the properties of the tree that can be queried were
  defined for debugging purposes.}

\subsection{The FILTER concept}
\label{sec:concepts}

The concept is documented in the file \texttt{beam\_search.hpp}. The
concept requires the following members:

\begin{itemize}
  
\item A type \NODE{} that specifies the type of a node in the \BST{}.
  
\item A type \LOCAL{} that specifies the type of the local evaluation
  of a \NODE.
  
\item A type \GLOBAL{} that specifies the type of the global
  evaluation of a \NODE.
  
\item The functions \texttt{get\_filter\_width()} and
  \texttt{get\_beam\_width()} that return respectively, the filter and
  beam widths of the tree.
  
\item The function \texttt{get\_root\_node()} that returns a reference
  to an immutable \NODE{} to be used as the root node of the tree.
  
\item The function \texttt{create\_nodes()} that creates the next set
  of leaf nodes.
  
\item The functions \texttt{local\_evaluation()} and
  \texttt{global\_evaluation()} to compute respectively, the local and
  global evaluations of a given node.
  
\item A pair of \texttt{compare()} functions to compare a pair of
  local or global evaluations.
  
\end{itemize}

No interface is required for \NODE{}, \LOCAL{}, and \GLOBAL. They are
just stored in the \BST{} and passed to the functions of the \FILTER{}
concept.

\subsection{The Beam Search Tree}

The implementation of the \BST{} can be found in the files
\texttt{beam\_search\_tree.hpp} The tree component is a wrapper of a
graph on the
\href{https://www.boost.org/doc/libs/1_68_0/libs/graph/doc/}{Boost
  Graph Library (BGL)}. The tree is stored as a bidirectional graph in
an adjacency list. An adjacency list is used to save space, as a tree
is an sparse graph. Edges are bidirectional to simplify traversal
algorithms. Each node has the following properties:
\begin{itemize}
\item An integer that uniquely identifies the node in the tree. The
  identifiers are assigned in creation order.
\item A \FILTER::\NODE.
\item A \FILTER::\LOCAL{} computed by calling the function
  \texttt{FILTER.local\_evaluation()} on the node's \FILTER::\NODE.
\item A \FILTER::\GLOBAL{} computed by calling the function
  \texttt{FILTER.global\_evaluation()} on the node's \FILTER::\NODE.
\end{itemize}

The tree component has a method to obtain the leaf with greatest
\FILTER::\GLOBAL{} according to the order given by the function
\texttt{FILTER.compare()}. This node is considered as the solution
obtained by the BSF.

\section{2D Single Knapsack filter}

The class \SBF{} is a model of the \FILTER{} concept. The class is
implemented in the files \texttt{skp\_filter.*}.

\begin{itemize}
  
\item The type \FILTER::\NODE{} is mapped to the class \SNODE. The
  \SNODE{} class is overviewed in Section~\ref{sec:node}.
  
\item The type \FILTER::\LOCAL{} is mapped to a tuple with values
  obtained from the particular configuration of the pieces placed in
  the node. The local evaluation of a \SNODE{} is explained in detail
  in Section~\ref{sec:local_evaluation}.
  
\item The type \FILTER::\GLOBAL{} is also mapped to the class
  \SNODE. It represents the projection of the given node. The
  projection procedure is explained in detail in
  Section~\ref{sec:local_evaluation}.
  
\item The functions to create, evaluate, and compare nodes are
  described in Sections \ref{sec:local_evaluation},
  \ref{sec:global_evaluation}, and \ref{sec:node_creation}.
    
\end{itemize}


\subsection{The rectangles}
\label{sec:rectangles}

The bin, the pieces, and the empty spaces left by the pieces placed on
the bin are modeled by several classes. The goal of the class
hierarchy is to reduce code repetition as much as possible.

\paragraph{Rectangle.}

A \RECTANGLE{} is defined by its width and its length. Both are
modeled with integers. The \RECTANGLE{} class is implemented in the
files \texttt{model/rectangle.*}.

\paragraph{Placeable rectangle.}

A \PRECTANGLE{} is a \RECTANGLE{} that is placed inside the
bin. Besides the properties of a \RECTANGLE, it contains the
coordinates of the bottom left coordinate of the rectangle. The
coordinates are referenced from the bottom left corner of the bin.

\paragraph{Piece.}

A \PIECE{} is a \PRECTANGLE{} that is placed on the bin.

\paragraph{Prototype.}

A \PROTOTYPE{} is a \RECTANGLE{} that models a row in the file
describing an instance of the problem. Besides the properties of a
\PIECE, it contains a unique id, the minimum and maximum number of
\PIECE s that can be placed, and the profit obtained by placing a
\PIECE. It also contains a counter with the number of \PIECE s already
placed, and the so called efficiency of the \PROTOTYPE{}, which is the
ratio between its profit and its area. The efficiency is stored as a
rational number improve efficiency and avoid precision issues.

\paragraph{Maximal rectangle}

A \MRECTANGLE{} is a \PRECTANGLE{} that is placed on an empty space
left by the \PIECE s placed on the bin, and does not intersects the
interior of any \PIECE. A \MRECTANGLE{} is maximal in the sense that
it does not contain a smaller \MRECTANGLE; i.e., their sides touch
either the border of the bin, or the border of a \PIECE.

\note{The names of the classes are inherited from older versions of
  the code. A better name might be used.}


\subsection{The evaluations}

We have developed several ways to evaluate an \SNODE. Those
evaluations are used to create local and global evaluations. All these
evaluation functions can be found in the files
\texttt{skp/skp\_evaluations.*}. The evaluation functions are briefly
described next. More details are given in Sections
\ref{sec:local_evaluation} and \ref{sec:global_evaluation}.

\paragraph{The efficiency vector.}

Evaluates the given \SNODE{} with a vector of \PROTOTYPE s. The vector
contains the maximum subset of \PROTOTYPE s such that all fit into one
\MRECTANGLE{} and the sum of their efficiencies is as big as
possible. The vector is sorted in decreasing order by efficiency.

To compare two efficiency vectors we use the accumulated efficiency of
the symmetric difference between the vectors. Let $V_a$ and $V_b$ be
two efficiency vectors, and $E(V)$ denote the accumulated efficiency
of the efficiency vector $V$. We compare $V_a$ and $V_b$ as follows:
\[
  V_a < V_b \Longleftrightarrow E(V_a \setminus V_b) < E(V_b \setminus
  V_a)
\]

\paragraph{The profit knapsack.}

Evaluates the given \SNODE{} with the maximum profit that can be
achieved with the available \PIECE s. The maximum profit is obtained
by solving the 0-1 knapsack problem
\begin{align*}
  \text{maximize} &\hspace{1em} \sum_{i=1}^{n} p_{i} x_{i}\\
  \text{subject to} &\hspace{1em} \sum_{i=1}^{n} w_{i} x_{i} \leq A
\end{align*}
where $x_{i} \in \{0,1\}$, $A$ is the available area of the bin, and
$p_i$, $w_i$ are respectively, the profit and the area of the ith
available \PIECE{}. To solve the problem we use a dynamic programming
algorithm. More information about can be found in
\texttt{algorithms/minknap.c}.

\paragraph{The best fit algorithm.}

Evaluates by projecting the given \SNODE{} using the best fit
algorithm. The returning evaluation is a pair containing the projected
\SNODE{} and its profit. Broadly speaking, to obtain the projected
node the best fit algorithm dynamically places as many \PIECE s as
possible on the bin. The order of placement is precomputed from the
set of \PROTOTYPE s. The algorithm is described in detail in
Section~\ref{sec:global_evaluation}.

\paragraph{Real waste.}

Computes the real waste of the given \SNODE.  The real waste is the
sum of the area of the \MRECTANGLE s that are not intersected by any
other \MRECTANGLE, and are not big enough to fit any available \PIECE.

\paragraph{Potential waste.}

Computes the potential waste of the given \SNODE. The potential waste
is the sum of the areas of the regions covered by only one
\MRECTANGLE{} where the last placed PIECE does not fit.

\subsection{The SKP Node}
\label{sec:node}

The class \SNODE{} is implemented in the files
\texttt{skp\_node.*}. The bin is modeled by a vector of \PIECE s with
no particular order. The node also contains the following properties:

\begin{itemize}
  
\item The set of \PROTOTYPE s from the problem instance with the
  \PIECE{} counter updated.
  
\item The maximum set of \MRECTANGLE s.
  
\item A set of properties regarding the sets of \PIECE s and
  \MRECTANGLE s: the total profit, the total area of the \PIECE s, and
  the waste, which is the sum of the areas of the \MRECTANGLE s.
  
\end{itemize}

The \SNODE{} class is meant to be an immutable information holder, so
it does not contain any methods to modify its properties.

\section{Local evaluation}
\label{sec:local_evaluation}

The local evaluation is a tuple made of the following five components
(in that order):

\begin{enumerate}
\item \label{enum:1} The efficiency of the last placed \PIECE{}.
\item A vector containing the \MRECTANGLE s sorted by decreasing area.
\item A vector containing the \MRECTANGLE s with the $y$-coordinate
  equal to the smallest $y$-coordinate among all \MRECTANGLE s. The
  \MRECTANGLE s are sorted by decreasing area.
\item The real waste of the \SNODE.
\item The potential waste of the \SNODE.
\end{enumerate}

To compare a pair of local evaluations we consider them as
five-dimensional vectors and perform a lexicographical
comparison. Numeric values (integers and rationals) are compared using
the less than ($<$) operator. Vectors are compared
lexicographically. In other words, we compare a pair of local
evaluations by first comparing the efficiency of the last placed
\PIECE{} (element~\ref{enum:1} of the tuple), and then using the
remaining four elements to break ties, if needed.

\section{Global Evaluation}
\label{sec:global_evaluation}

The global evaluation is the \SNODE{} obtained by using the best fit
algorithm to the given \SNODE{}. We outline the algorithm next:

\begin{enumerate}
  
\item \label{step:1} Sort the set of \MRECTANGLE s by non-decreasing
  $y$ coordinate of its bottom left corner. Solve ties by
  non-decreasing $x$ coordinate.
  
\item Traverse the sorted set of \MRECTANGLE s. For each rectangle:
  \begin{enumerate}
  \item Find the best \PROTOTYPE{} fitting the \MRECTANGLE{} according
    the given comparison function.
  \item Place a \PIECE{} of the selected \PROTOTYPE{} aligned with
    bottom left corner of the \MRECTANGLE{}.
  \item Update the set of \MRECTANGLE s.
  \end{enumerate}
  
\item If there is a \PROTOTYPE{} with available \PIECE s and there is
  still space in the bin to place at least one \PIECE{}, repeat the
  procedure from Step \ref{step:1}. Otherwise, end the algorithm.
  
\end{enumerate}

\section{Node creation}
\label{sec:node_creation}

To create the next level of leaves, the \FILTER{} concept defines the
method
\begin{lstlisting}[language=C++]
void
FILTER::create_nodes(NODES& nodes)
\end{lstlisting}
where \texttt{NODES} is a vector containing the \FILTER::\NODE s that
form the previous set of leaves, and methods to create the new
leaves. We outline next the leave creation algorithm:

\begin{enumerate}
  
\item Filter the set of leaves to keep only those where at least one
  piece can be placed on the bin.
  
\item Sort the set of surviving leaves in increasing order by local
  evaluation.
  
\item Traverse the sorted set of leaves.
  \begin{enumerate}
    
  \item Create children nodes of the current leaf node. Children are
    created by placing one \PIECE{} of each \PROTOTYPE{} in the
    smallest \MRECTANGLE{} fitting the \PIECE. The \PIECE{} is placed
    aligned to each of the four corners of the \MRECTANGLE{} such that
    the \PIECE{} touches at least two other \PIECE s.
    
  \item Filter symmetric nodes. Two \SNODE s are symmetric if their
    sets of placed \PIECE s are equal, and their sets of \MRECTANGLE s
    can be paired in a perfect matching such that the rectangles of
    every matched pair have equal length and height.

    Test each created child leaf and remove it from the set if it is
    symmetric to a child leaf created previously.
    
  \item Filter the surviving leaf children so the set has at most
    \FILTER::\texttt{get\_filter\_width()} elements.
  \end{enumerate}
\end{enumerate}

\section{Example}

We show next a runnign example of the current implementation. We use
the instance beasley8 with $\alpha = 2$ and $\beta = 3$. The instance
and its optimal profit are shown in Figure~\ref{fig:beasley_8}.

\begin{figure}[t]
  \centering
  \begin{tabular}[b]{|c|c|c|c|c|}
    id & length & height & profit & limit\\
    \noalign{\smallskip} \hline \noalign{\smallskip}
    0& 15& 3& 110& 1\\ \hline{}
    1& 6& 6& 93& 3\\ \hline{}
    2& 13& 1& 38& 2\\ \hline{}
    3& 8& 7& 144& 2\\ \hline{}
    4& 18& 5& 185& 3\\ \hline{}
    5& 12& 4& 78& 1\\ \hline{}
    6& 8& 3& 25& 1\\ \hline
  \end{tabular}
  \includegraphics[width=0.4\textwidth]{beasley8_optimal}
  \caption{Beasley 8 instance}
  \label{fig:beasley_8}
\end{figure}

\paragraph{1. Initialization.}

The tree is initalized with a dummy node containing an empty bin.

\paragraph{2. The main cycle.}

The Beam Search main cycle maintains two node vectors. The vector
\BF{} contains the (at most $\beta$) leaf nodes of the \BST{}. The
vector \FF{} contains the (at most $\alpha \times \beta$) children of
the nodes in \BF{} from which we will fillter the node to be stored in
the \BF{} for the next cycle.

\paragraph{2.1. Fill \FF.}

The function \texttt{FILTER::create\_nodes} is called to fill
\FF. The node After this function is called the tree looks like Figure blah.



% \bibliographystyle{plain}
% \bibliography{bibfile}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
