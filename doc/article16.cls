\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{article16}

%-------------------- This is based on the article class ----------------

\PassOptionsToClass{onecolumn}{article}
\PassOptionsToClass{twoside}{article}
\PassOptionsToClass{a4paper}{article}

\LoadClass{article}

\RequirePackage{theorem,graphicx,amssymb,amsmath,fancyhdr,url}

%------------------ Defining the most common environments ---------------

\theorembodyfont{\slshape}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{cor}[theorem]{Corollary}
\newtheorem{prop}[theorem]{Proposition}
\newtheorem{invar}{Invariant}
\newtheorem{obs}{Observation}
\newtheorem{conj}{Conjecture}
\newtheorem{defini}{Definition}
\newtheorem{claim}{Claim}

\def\QED{\ensuremath{{\square}}}
\def\markatright#1{\leavevmode\unskip\nobreak\quad\hspace*{\fill}{#1}}
\newenvironment{proof}
 {\begin{trivlist}\item[\hskip\labelsep{\bf Proof.}]}
 {\markatright{\QED}\end{trivlist}}

%------------------------------------------------------------------------
\setlength{\textheight}{250mm}
\setlength{\textwidth}{180mm}
\setlength{\oddsidemargin}{0pt}
\setlength{\topmargin}{-20pt}
\setlength{\headsep}{20pt}
\setlength{\textwidth}{160mm}
\setlength{\evensidemargin}{0pt}
\setlength{\headheight}{12pt}




\font\tensfb = cmssbx10
\newcommand{\@titlestyle}{\large\tensfb}

\let\ps@plain\ps@fancy

\renewcommand{\@maketitle}{%
 \newpage
 \null
 \vskip 2em%
 \begin{center}%
   \let \footnote \thanks
   {\vspace{-.2in}\LARGE\sffamily\bfseries \@title \par}%
   \vskip 1.5em%
   {\large
     \lineskip .5em%
     \begin{tabular}[t]{c}%
       \@author
     \end{tabular}\par}%
   \vskip 1em%
   {\Large \@date}%
 \end{center}%
 \par
 \vskip 3em}

\renewenvironment{theindex}
              {\if@twocolumn
                 \@restonecolfalse
               \else
                 \@restonecoltrue
               \fi
               \columnseprule \z@
               \columnsep 35\p@
               \twocolumn[\section*{\indexname}]%
               \@mkboth{\MakeUppercase\indexname}%
                       {\MakeUppercase\indexname}%
               \thispagestyle{fancy}\parindent\z@
               \parskip\z@ \@plus .3\p@\relax
               \let\item\@idxitem}
              {\if@restonecol\onecolumn\else\clearpage\fi}

\renewcommand\section{\@startsection {section}{1}{\z@}{-3.5ex plus
   -1ex minus -.2ex}{2.3ex plus .2ex}{\@titlestyle}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}{-3.25ex
   plus -1ex minus -.2ex}{1.5ex plus .2ex}{\@titlestyle}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}{-3.25ex
   plus -1ex minus -.2ex}{1.5ex plus .2ex}{\@titlestyle}}
\renewcommand\paragraph{\@startsection {paragraph}{4}{\z@}{3.25ex plus
   1ex minus .2ex}{-1em}{\@titlestyle}}
\renewcommand\subparagraph{\@startsection
 {subparagraph}{4}{\parindent}{3.25ex plus 1ex minus
   .2ex}{-1em}{\@titlestyle}}

\renewcommand\abstract{\if@twocolumn
\section*{Abstract}
\else \small
\begin{center}
{\sffamily\bfseries Abstract\vspace{-.5em}\vspace{0pt}}
\end{center}
\quotation
\fi}

\def\endabstract{\if@twocolumn\else\endquotation\fi}

\date{}

\renewcommand{\textfraction}{0.05}
\renewcommand{\bottomfraction}{0.95}
\renewcommand{\topfraction}{0.95}
\renewcommand{\dbltopfraction}{0.95}
\renewcommand{\dblfloatpagefraction}{0.8}
\setcounter{topnumber}{3}

\newcommand{\l@day}{\@dottedtocline{1}{0em}{0em}}
\newcommand{\l@session}{\@dottedtocline{1}{0em}{1in}}
\newcommand{\l@algorithm}{\@dottedtocline{1}{0em}{1in}}
\renewcommand{\@dotsep}{500}

%--------------------------- Headers and Footers ------------------------

\fancypagestyle{plain}{
\fancyhead[RO]{\sf }
\fancyhead[LE]{\sf }
\fancyfoot[L]{\sf \scriptsize{}}
}


\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyhead[RO]{\sf {\footnotesize } \textit{Marta Cabo and Carlos
    Alegr\'{i}a}}
\fancyhead[LE]{\sf \textit{Beam search implementation for the 2D
    Single Knapsack Problem, 2019}}

\renewcommand{\headrulewidth}{1pt}

\endinput


